package app.business.testcheckout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.security.ProviderInstaller;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.MobileNumber;
import xente.business.models.SavedPayment;
import xente.business.models.XenteCashback;
import xente.business.transaction.CreditParty;
import xente.business.transaction.DebitParty;
import xente.business.transaction.MetaData;
import xente.business.transaction.TransactionRequest;
import xente.business.utils.AppUtils;
import xente.business.utils.Constants;
import xente.business.utils.TXProcedure;
import xente.business.xentecheckout.PaymentList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ProviderInstaller.ProviderInstallListener {

    private int REQUEST_CODE = 0;

    private String PAYMENT_PROVIDER = null;
    private CartInstance cartInstance;
    private FirebaseRemoteConfig firebaseRemoteConfig;
    private String COUNTRY = "UG";
    private String PAYMENT_PROVIDER_KEY = "payment_source";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ProviderInstaller.installIfNeededAsync(MainActivity.this, MainActivity.this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button checkoutButton = (Button)findViewById(R.id.checkout_button);
        checkoutButton.setOnClickListener(this);

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        firebaseRemoteConfig.setConfigSettings(configSettings);

        AppUtils.showToolTip(this, checkoutButton, "Hello world");
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        getPaymentProvider();
    }

    public void getPaymentProvider(){

        cartInstance = new CartInstance();
        cartInstance.setAmount(new BigDecimal(100));
        cartInstance.setFlatAmount(new BigDecimal(100));
        cartInstance.setRequestId(UUID.randomUUID().toString());
        cartInstance.setCurrency("USD");
        cartInstance.setProductInformation(getTransactionPayload());

        String paymentProvider = firebaseRemoteConfig.getString(PAYMENT_PROVIDER_KEY);
        Log.d("flutterwave", "Payment:"+paymentProvider);
        if(paymentProvider != null){

            try {
                JSONObject object = new JSONObject(paymentProvider);

                String PAYMENT_PROVIDER = object.getString(COUNTRY);

                if(PAYMENT_PROVIDER != null){


                    Intent intent = new Intent(this, PaymentList.class);
                    CardModel cardModel = new CardModel();
                    cardModel.setEmail("test@mail.com");
                    cardModel.setPhoneNumber("0787000002");
                    cardModel.setFirstName("Fat");
                    cardModel.setLastName("Boy");
                    cardModel.setCountry(COUNTRY);
                    cardModel.setPaymentProvider(PAYMENT_PROVIDER);
                    cardModel.setCity("Kampala");
                    cardModel.setState("Central");
                    cardModel.setCountry("UG");
                    cardModel.setPostal("8882");
                    cardModel.setAddress("Kampala");

                    intent.putExtra(Constants.CARD_MODEL, (Serializable)cardModel);
                    startActivityForResult(intent, REQUEST_CODE);


                }else{
                    Toast.makeText(this, "Couldn't retreive any payment provider.", Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                Toast.makeText(this, "Couldn't retreive any payment provider.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_CODE){

                Intent intent = new Intent(this, CheckOut.class);
                CardModel cardModel = null;
                SavedPayment savedPayment = null;
                MobileNumber mobileNumber = null;
                XenteCashback xenteCashback = null;
                TXProcedure procedure = null;

                try{
                    if((CardModel)data.getSerializableExtra(Constants.CARD_MODEL) instanceof CardModel){
                        cardModel = (CardModel) data.getSerializableExtra(Constants.CARD_MODEL);
                        procedure = (TXProcedure) data.getSerializableExtra(Constants.TX_PROCEDURE);

                        intent.putExtra(Constants.CARD_MODEL,  (Serializable)cardModel);
                        intent.putExtra(Constants.TX_PROCEDURE, (Serializable)procedure);
                        intent.putExtra(Constants.CART_INSTANCE, (Serializable)cartInstance);
                    }


                }catch (ClassCastException e){

                    if((SavedPayment)data.getSerializableExtra(Constants.CARD_MODEL) instanceof SavedPayment){
                        savedPayment = (SavedPayment)data.getSerializableExtra(Constants.CARD_MODEL);
                        procedure = (TXProcedure) data.getSerializableExtra(Constants.TX_PROCEDURE);

                        intent.putExtra(Constants.CARD_MODEL,  (Serializable)savedPayment);
                        intent.putExtra(Constants.TX_PROCEDURE, (Serializable)procedure);
                        intent.putExtra(Constants.CART_INSTANCE, (Serializable)cartInstance);


                    }
                }

                if((MobileNumber)data.getSerializableExtra(Constants.MOBILE_MODEL) instanceof MobileNumber){
                    mobileNumber = (MobileNumber)data.getSerializableExtra(Constants.MOBILE_MODEL);

                    intent.putExtra(Constants.CART_INSTANCE, (Serializable)cartInstance);
                    intent.putExtra(Constants.MOBILE_MODEL, (Serializable)mobileNumber);
                }
                if((XenteCashback)data.getSerializableExtra(Constants.CASHBACK_MODEL) instanceof XenteCashback){
                    xenteCashback = (XenteCashback)data.getSerializableExtra(Constants.CASHBACK_MODEL);

                    intent.putExtra(Constants.CART_INSTANCE, (Serializable)cartInstance);
                    intent.putExtra(Constants.CASHBACK_MODEL, (Serializable)xenteCashback);
                }

                startActivity(intent);
            }
        }
    }

    @Override
    public void onProviderInstalled() {

    }

    @Override
    public void onProviderInstallFailed(int i, Intent intent) {

    }

    @Override
    public void onResume(){
        super.onResume();
        firebaseRemoteConfig.setDefaults(R.xml.remote_configuration_defaults);
    }

    private String getTransactionPayload(){

        Gson gGson  = new GsonBuilder().setPrettyPrinting().create();

        TransactionRequest transactionRequest = new TransactionRequest();

        transactionRequest.setAmount(new BigDecimal(100.5));
        transactionRequest.setServicingIdentity("WEBAPI");
        transactionRequest.setRequestDate(new Date());
        transactionRequest.setType("disbursement");
        transactionRequest.setCurrency("UGX");
        transactionRequest.setRequestingOrganisationTransactionReference("a14fe3f3cc0b238a0e66e5a8e44460fb");
        transactionRequest.setDescriptionText("Payment test for IP2");

        transactionRequest.setMetaData(getMetaData());
        transactionRequest.setCreditParty(getCreditParty());
        transactionRequest.setDebitParty(getDebitParty());

        String s = gGson.toJson(transactionRequest);
        return s;
    }

    private CreditParty getCreditParty(){
        CreditParty creditParty = new CreditParty();
        creditParty.setAccountId("256784703425");
        creditParty.setOrganizationId(UUID.randomUUID().toString());
        return creditParty;
    }

    private DebitParty getDebitParty(){
        DebitParty debitParty = new DebitParty();
        debitParty.setAccountId("0784703425");
        debitParty.setOrganizationId(UUID.randomUUID().toString());
        return debitParty;
    }

    private MetaData getMetaData(){

        HashMap<String, Object> paymentMap = new HashMap<>();
        paymentMap.put("accountId", "0784703425");
        paymentMap.put("msisdn", "0772613954");

        HashMap<String, Object> productMap = new HashMap<>();
        productMap.put("productId", "MTNAIRTIMEUG");
        productMap.put("amount", 500);

        HashMap<String, Object> channelMap = new HashMap<>();
        channelMap.put("channel", "xenteandroidapp");

        HashMap<String, Object> requestMap = new HashMap<>();
        requestMap.put("customer_name", "Fat boy");

        MetaData metaData = new MetaData();
        metaData.setBatchId(UUID.randomUUID().toString());
        metaData.setCountryCode("256");
        metaData.setCreatedOn(new Date());
        metaData.setSubscriptionId(UUID.randomUUID().toString());
        metaData.setChannelReference(channelMap);
        metaData.setPaymentReference(paymentMap);
        metaData.setProductReference(productMap);
        metaData.setRequestReference(requestMap);
        return metaData;
    }
}
