package app.business.testcheckout;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.io.Serializable;

import xente.business.fragments.CheckoutFragment;
import xente.business.fragments.OTPBottomFragment;
import xente.business.fragments.SlidingWebViewFragment;
import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.MobileNumber;
import xente.business.models.SavedPayment;
import xente.business.models.XenteCashback;
import xente.business.utils.Constants;
import xente.business.utils.TXProcedure;

public class CheckOut extends AppCompatActivity implements SlidingWebViewFragment.ActivityCallback,
        OTPBottomFragment.OTPFragmentCallback, CheckoutFragment.CheckoutCallack {

    private CardModel cardModel;
    private TXProcedure procedure;
    private CartInstance cartInstance;
    private MobileNumber mobileNumber;
    private XenteCashback xenteCashback;

    private CheckoutFragment checkoutFragment;
    private SavedPayment savedPayment;
    private String TAG = "CHECKOUT_TAG";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getIntent() != null){
            try{
                if((CardModel)getIntent().getSerializableExtra(Constants.CARD_MODEL) instanceof CardModel){
                    cardModel = (CardModel) getIntent().getSerializableExtra(Constants.CARD_MODEL);
                    procedure = (TXProcedure)getIntent().getSerializableExtra(Constants.TX_PROCEDURE);
                    cartInstance = (CartInstance)getIntent().getSerializableExtra(Constants.CART_INSTANCE);
                }
            }catch (ClassCastException e){
                if((SavedPayment)getIntent().getSerializableExtra(Constants.CARD_MODEL) instanceof SavedPayment){
                    savedPayment = (SavedPayment)getIntent().getSerializableExtra(Constants.CARD_MODEL);
                    procedure = (TXProcedure)getIntent().getSerializableExtra(Constants.TX_PROCEDURE);
                    cartInstance = (CartInstance)getIntent().getSerializableExtra(Constants.CART_INSTANCE);
                }
            }

            if((MobileNumber)getIntent().getSerializableExtra(Constants.MOBILE_MODEL) instanceof MobileNumber){
                mobileNumber = (MobileNumber)getIntent().getSerializableExtra(Constants.MOBILE_MODEL);
                cartInstance = (CartInstance)getIntent().getSerializableExtra(Constants.CART_INSTANCE);
            }
            if((XenteCashback)getIntent().getSerializableExtra(Constants.CASHBACK_MODEL) instanceof XenteCashback){
                xenteCashback = (XenteCashback)getIntent().getSerializableExtra(Constants.CASHBACK_MODEL);
                cartInstance = (CartInstance)getIntent().getSerializableExtra(Constants.CART_INSTANCE);

            }

        }

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.confirm_payment));
        }
        checkoutFragment = new CheckoutFragment();

        Bundle bundle = new Bundle();

        if(cardModel != null){
            bundle.putSerializable(Constants.CARD_MODEL, (Serializable)cardModel);
        }else if(savedPayment != null){
            bundle.putSerializable(Constants.CARD_MODEL, (Serializable)savedPayment);
        }else if(mobileNumber != null){
            bundle.putSerializable(Constants.MOBILE_MODEL, (Serializable)mobileNumber);
        }else if(xenteCashback != null){
            bundle.putSerializable(Constants.CASHBACK_MODEL, (XenteCashback)xenteCashback);
        }

        bundle.putSerializable(Constants.TX_PROCEDURE, (Serializable)procedure);
        bundle.putSerializable(Constants.CART_INSTANCE, (Serializable)cartInstance);

        checkoutFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.check_out_container, checkoutFragment, TAG).commit();

    }

    @Override
    public void cancelWebView(String url) {
        CheckoutFragment fragment = (CheckoutFragment)getSupportFragmentManager().findFragmentByTag(TAG);
        if(fragment != null && fragment.isVisible()){
            fragment.dismissWebView(url);
        }
    }

    @Override
    public void UIOTPOkActionClicked(boolean clicked, String otp) {
        CheckoutFragment fragment = (CheckoutFragment)getSupportFragmentManager().findFragmentByTag(TAG);
        if(fragment != null && fragment.isVisible()){
            fragment.UIOTPOkActionClicked(clicked, otp);
        }
    }


    @Override
    public void showProgressSheet() {

    }
}
