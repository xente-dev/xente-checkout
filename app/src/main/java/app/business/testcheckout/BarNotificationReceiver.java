package app.business.testcheckout;



import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import xente.business.utils.Constants;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;

/**
 * Created by macosx on 20/03/2018.
 */

public class BarNotificationReceiver extends BroadcastReceiver {

    private final String CHANNEL_ID = "xente_channel_id";
    private int NOTIFICATION_ID;
    private String ACTION_CONTACT_SUPPORT = "xente.business.contact.support";


    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("flutterwave", "Notification received from app");
        String message = intent.getStringExtra(Constants.NOTIFICATION_STRING);
        int status = intent.getIntExtra(Constants.NOTIFICATION_STATUS, -1);

        NOTIFICATION_ID = getNotificationId();
        sendNotification(context, message, status);
    }

    private void sendNotification(Context context, String message, int status){

        String contentTile = context.getString(xente.business.xentecheckout.R.string.xente);

        Log.d("flutterwave", "sending bar notification");
        Intent contactSupportIntent = new Intent(context, CheckOut.class);
        contactSupportIntent.setAction(ACTION_CONTACT_SUPPORT);
        contactSupportIntent.putExtra(EXTRA_NOTIFICATION_ID, 0);
        PendingIntent supportPendingIntent =
                PendingIntent.getBroadcast(context, 0, contactSupportIntent, 0);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "XENTE")
                //.setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(contentTile)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        if(status != 0){
            mBuilder.addAction(R.drawable.notification_support_button, context.getString(R.string.contact_support),
                    supportPendingIntent);
        }



        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(NOTIFICATION_ID, mBuilder.build());


    }

    public int getNotificationId(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("mmss");
        return Integer.parseInt(simpleDateFormat.format(new Date()));
    }

}
