package xente.business.interfaces;

import xente.business.models.NetworkResponse;

/**
 * Created by Intel World on 20/02/2018.
 */
public interface NetworkConnectionCallback {
    void callback(NetworkResponse networkResponse);
}
