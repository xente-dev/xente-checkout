package xente.business.interfaces;

import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.MobileNumber;

/**
 * Created by macosx on 21/03/2018.
 */

public interface CallIP2Service {

    public <T> void requestIP2Payment(CartInstance cartInstance, T callback);
    public <T> void requestIP2Payment(Object object, CartInstance cartInstance, T callback);
}
