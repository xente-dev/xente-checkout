package xente.business.interfaces;

import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.FLWChargeResponse;

/**
 * Created by macosx on 22/02/2018.
 */

public interface Flutterwave {

    public <T> void requestFLWCardPayment(CardModel cardModel, CartInstance cartInstance, T callback);
    public <T> void flutterwaveValidateCardChargeOTP(String transactionRef, int opt, T callback);
}
