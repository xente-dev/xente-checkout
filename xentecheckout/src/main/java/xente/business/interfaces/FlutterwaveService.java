package xente.business.interfaces;

/**
 * Created by macosx on 26/02/2018.
 */

public interface FlutterwaveService {
    public <T> void postFlutterwaveTransactionToService(String servicePayload, T callback);
    public <T> void postFlutterwaveValidatationToService(String url, T callback);
    public <T> void postFlutterwaveTokenizedTransactionToService(String servicePayload, T callback);
}
