package xente.business.interfaces;

import xente.business.models.CardModel;
import xente.business.models.CartInstance;

/**
 * Created by macosx on 22/02/2018.
 */

public interface Cybersource {

    public <T> void requestCybersourceCardPayment(CardModel cardModel, CartInstance cartInstance, T callback);

}
