package xente.business.transaction;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by macosx on 22/03/2018.
 */

public class TransactionRequest {

    public TransactionRequest(){

    }

    @NonNull
    @SerializedName("amount")
    private BigDecimal amount;

    @NonNull
    @SerializedName("currency")
    private String currency;

    @SerializedName("requestDate")
    private Date requestDate;

    @NonNull
    @SerializedName("type")
    private String type;

    @SerializedName("descriptionText")
    private String descriptionText;

    @SerializedName("requestingOrganisationTransactionReference")
    private String requestingOrganisationTransactionReference;

    @SerializedName("servicingIdentity")
    private String servicingIdentity;

    @SerializedName("debitParty")
    private DebitParty debitParty;

    @SerializedName("creditParty")
    private CreditParty creditParty;

    @SerializedName("metadata")
    private MetaData metaData;

    @NonNull
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(@NonNull BigDecimal amount) {
        this.amount = amount;
    }

    @NonNull
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(@NonNull String currency) {
        this.currency = currency;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    @NonNull
    public String getType() {
        return type;
    }

    public void setType(@NonNull String type) {
        this.type = type;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public String getRequestingOrganisationTransactionReference() {
        return requestingOrganisationTransactionReference;
    }

    public void setRequestingOrganisationTransactionReference(String requestingOrganisationTransactionReference) {
        this.requestingOrganisationTransactionReference = requestingOrganisationTransactionReference;
    }

    public String getServicingIdentity() {
        return servicingIdentity;
    }

    public void setServicingIdentity(String servicingIdentity) {
        this.servicingIdentity = servicingIdentity;
    }

    public DebitParty getDebitParty() {
        return debitParty;
    }

    public void setDebitParty(DebitParty debitParty) {
        this.debitParty = debitParty;
    }

    public CreditParty getCreditParty() {
        return creditParty;
    }

    public void setCreditParty(CreditParty creditParty) {
        this.creditParty = creditParty;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
