package xente.business.transaction;

import com.google.gson.annotations.SerializedName;

/**
 * Created by macosx on 22/03/2018.
 */

public class CreditParty {

    @SerializedName("accountId")
    private String accountId;

    @SerializedName("organizationId")
    private String organizationId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }
}
