package xente.business.transaction;

import android.webkit.HttpAuthHandler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by macosx on 22/03/2018.
 */

public class MetaData {

    @SerializedName("createdOn")
    private Date createdOn;

    @SerializedName("subscriptionId")
    private String subscriptionId;

    @SerializedName("customerId")
    private String customerId;

    @SerializedName("countryCode")
    private String countryCode;

    @SerializedName("batchId")
    private String batchId;

    @SerializedName("paymentReference")
    private HashMap<String, Object> paymentReference;

    @SerializedName("productReference")
    private HashMap<String, Object> productReference;

    @SerializedName("channelReference")
    private HashMap<String, Object> channelReference;

    @SerializedName("requestReference")
    private HashMap<String, Object> requestReference;


    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }


    public HashMap<String, Object> getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(HashMap<String, Object> paymentReference) {
        this.paymentReference = paymentReference;
    }

    public HashMap<String, Object> getProductReference() {
        return productReference;
    }

    public void setProductReference(HashMap<String, Object> productReference) {
        this.productReference = productReference;
    }

    public HashMap<String, Object> getChannelReference() {
        return channelReference;
    }

    public void setChannelReference(HashMap<String, Object> channelReference) {
        this.channelReference = channelReference;
    }

    public HashMap<String, Object> getRequestReference() {
        return requestReference;
    }

    public void setRequestReference(HashMap<String, Object> requestReference) {
        this.requestReference = requestReference;
    }
}
