package xente.business.services;

import android.app.Activity;
import android.app.IntentService;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.json.JSONException;
import org.json.JSONObject;

import xente.business.utils.Constants;
import xente.business.xentecheckout.R;

/**
 * Created by macosx on 05/03/2018.
 */

public class NotificationService extends Service implements MqttCallback {

    private int NOTIFICATION_MESSAGE;
    private String BROKER = "13.93.108.25";
    private String PORT = "1883";
    private String PROTOCOL ="tcp://";
    private String COLON_SEPARATOR = ":";
    private String CLIENT_ID;
    private String TOPIC;
    private MemoryPersistence persistence;
    private MqttDefaultFilePersistence file;
    private MqttClient client;
    private String BROKER_URL;
    private int QOS = 1;

    private boolean messageDelivered = false;
    private boolean timerElapsed = false;
    private boolean timerStoppedExplicitly = false;
    private boolean amAlive;

    private String REQUEST_ID;
    public static final String BROADCAST_ACTION = "xente.business.services.Broadcast";

    public IBinder iBinder = new NotificationBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void connectionLost(Throwable cause) {
        if(!messageDelivered && !timerElapsed) {
            try {
                client.connect();
            } catch (MqttSecurityException e) {
                sendBroadcast(getApplicationContext().getString(R.string.processing_message_overdue), -1);
                this.stopSelf();

            } catch (MqttException e) {
                sendBroadcast(getApplicationContext().getString(R.string.processing_message_overdue), -1);
                this.stopSelf();
            }
        }
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {

        String receivedRequestId = null;
        try{
            JSONObject object = new JSONObject(new String(message.getPayload()));
            final int statusCode = object.getInt("StatusCode");
            final String statusMessage = object.getString("StatusMessage");
            receivedRequestId = object.getString("RequestId");

            //sendNotification(statusMessage);

            if (receivedRequestId.equals(REQUEST_ID)) {
                messageDelivered = true;

                sendBroadcast(statusMessage, statusCode);
            }
        }catch(JSONException e){
            if(receivedRequestId.equals(REQUEST_ID)){
                sendBroadcast(getApplicationContext().getString(R.string.processing_message_overdue), -1);
                this.stopSelf();
            }
        }

    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }

    public class NotificationBinder extends Binder {
        public NotificationService getService() {
            return NotificationService.this;
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        TOPIC = intent.getStringExtra(Constants.TOPIC);
        REQUEST_ID = intent.getStringExtra(Constants.REQUEST_ID);

        persistence = new MemoryPersistence();
        file = new MqttDefaultFilePersistence();
        BROKER_URL = PROTOCOL + BROKER + COLON_SEPARATOR + PORT;
        CLIENT_ID = TOPIC+"_XENTE";
        TOPIC = TOPIC+"_NOTIFICATION";


        try {
            client = new MqttClient(BROKER_URL, CLIENT_ID, persistence);

            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            client.setCallback(this);
            client.connect(connOpts);
            client.subscribe(TOPIC, QOS);
            countDownTimer.start();

        } catch (MqttException e) {
            sendBroadcast(getApplicationContext().getString(R.string.processing_message_overdue), -1);
            this.stopSelf();
        }

        return Service.START_NOT_STICKY;
    }

    private CountDownTimer countDownTimer = new CountDownTimer(20000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {

            timerElapsed = true;

            if(!timerStoppedExplicitly){

                sendBarNotificationToReceiver(getApplicationContext().getString(R.string.processing_message_overdue), -1);
                sendBroadcast(getApplicationContext().getString(R.string.processing_message_overdue), -1);
                NotificationService.this.stopSelf();

            }
        }
    };

    private void sendBroadcast(String message, int status){

        if(amAlive){

            Intent intent = new Intent();
            intent.setAction(BROADCAST_ACTION);
            intent.putExtra(Constants.NOTIFICATION_STRING, message);
            intent.putExtra(Constants.NOTIFICATION_STATUS, status);
            sendBroadcast(intent);
        }else{

            saveToPreferences(message, status);
        }

    }

    private void sendBarNotificationToReceiver(String message, int status){
        final Intent intent=new Intent();
        intent.setAction("barnotification.handler");
        intent.putExtra(Constants.NOTIFICATION_STRING, message);
        intent.putExtra(Constants.NOTIFICATION_STATUS, status);
        intent.addFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        intent.setComponent(new ComponentName("app.business.testcheckout", "app.business.testcheckout.BarNotificationReceiver"));
        sendBroadcast(intent);
    }

    private void saveToPreferences(String message, int status){

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(Constants.NOTIFICATION_MESSAGE_STORE, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(Constants.NOTIFICATION_STRING, message);
        editor.putInt(Constants.NOTIFICATION_STATUS, status);
        editor.apply();

    }
    public void amAlive(){
        amAlive = true;
    }
    public void amDead(){
        amAlive = false;
    }
}
