package xente.business.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.apache.commons.lang.time.StopWatch;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import xente.business.utils.Constants;

/**
 * Created by macosx on 05/03/2018.
 */

public class MqttService extends Service implements MqttCallback {


    private String BROKER = "13.93.108.25";
    private String PORT = "1883";
    private String PROTOCOL ="tcp://";
    private String COLON_SEPARATOR = ":";
    private String CLIENT_ID;
    private String CARD_ID;
    private MemoryPersistence persistence;
    private MqttDefaultFilePersistence file;
    private MqttClient client;
    private String BROKER_URL;
    private int QOS = 1;
    private int messageType;

    private long TOKENIZATON_WAIT_TIME = 120000;
    private long TOKENIZATION_REFRESH_WAIT_TIME = 180000;
    private long WAIT_TIME;

    private String dataStore = "/home/iwadmin/hivemq/datastore";

    private boolean messageDelivered = false;
    private boolean timerElapsed = false;
    private boolean timerStoppedExplicitly = false;

    private IBinder iBinder = new MBinder();


    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d("flutterwave", "service started");
        String topic = intent.getStringExtra(Constants.TOPIC);
        messageType = intent.getIntExtra(Constants.MESSAGE_TYPE, 0);
        timerStoppedExplicitly = false;


        persistence = new MemoryPersistence();
        file = new MqttDefaultFilePersistence();
        BROKER_URL = PROTOCOL + BROKER + COLON_SEPARATOR + PORT;
        CLIENT_ID = topic+"_XENTE";

        switch (messageType){

            case Constants.TOKENIZATION_MESSAGE:
                WAIT_TIME = TOKENIZATON_WAIT_TIME;
                break;


            case Constants.TOKE_REFRESH_MESSAGE:
                WAIT_TIME = TOKENIZATION_REFRESH_WAIT_TIME;
                CARD_ID = intent.getStringExtra(Constants.CARD_ID);
                break;

        }

        try {
            client = new MqttClient(BROKER_URL, CLIENT_ID, persistence);

            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);

            client.setCallback(this);
            client.connect(connOpts);
            client.subscribe(topic, QOS);
            countDownTimer.start();

        } catch (MqttException e) {
            sendOverdueMessageBroadcast();
            this.stopSelf();
        }

        return Service.START_NOT_STICKY;
    }


    @Override
    public void connectionLost(Throwable arg0) {

        if(!messageDelivered && !timerElapsed) {
            try {
                client.connect();
            } catch (MqttSecurityException e) {
                sendOverdueMessageBroadcast();
                this.stopSelf();

            } catch (MqttException e) {
                sendOverdueMessageBroadcast();
                this.stopSelf();
            }
        }
    }
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {

        messageDelivered = true;
        try {

            timerStoppedExplicitly = true;
            countDownTimer.cancel();

            String receivedMessage = new String(mqttMessage.getPayload());
            Log.d("flutterwave", "Message"+receivedMessage);

            Intent intent = new Intent("MqttReceiver");
            intent.putExtra(Constants.MQTT_MESSAGE, receivedMessage);
            intent.putExtra(Constants.MESSAGE_TYPE, messageType);

            if(CARD_ID != null){
                intent.putExtra(Constants.CARD_ID, CARD_ID);
            }

            sendBroadcast(intent);

            client.disconnect();
            this.stopSelf();

        } catch (MqttException e) {
            sendOverdueMessageBroadcast();
            this.stopSelf();
        }

    }

    private CountDownTimer countDownTimer = new CountDownTimer(60000, 1000) {


        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {

            Log.d("flutterwave", "Timer went off");

            timerElapsed = true;

            if(!timerStoppedExplicitly){



                switch (messageType)
                {
                    case Constants.TOKE_REFRESH_MESSAGE:
                        sendOverdueMessageBroadcast();
                        break;
                }

                MqttService.this.stopSelf();

            }


        }
    };

    private void sendOverdueMessageBroadcast(){
        Intent intent = new Intent("MqttReceiver");
        intent.putExtra(Constants.MESSAGE_TYPE, Constants.OVER_DUE_TOKE_REFRESH_MESSAGE);
        intent.putExtra(Constants.CARD_ID, CARD_ID);
        sendBroadcast(intent);
    }

    public class MBinder extends Binder {
        public MqttService getService() {
            return MqttService.this;
        }
    }

}
