package xente.business.services;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import xente.business.utils.AppUtils;
import xente.business.utils.Constants;
import xente.business.utils.EncryptorDecryptor;

/**
 * Created by macosx on 05/03/2018.
 */

public class MqttReceiver extends BroadcastReceiver {

    private String cardId;
    private AppUtils appUtils;

    @Override
    public void onReceive(Context context, Intent intent) {

        int messageType = intent.getIntExtra(Constants.MESSAGE_TYPE, 0);
        String message = intent.getStringExtra(Constants.MQTT_MESSAGE);

        appUtils = new AppUtils(context);

        switch (messageType){
            case Constants.TOKENIZATION_MESSAGE:
                appUtils.saveTokenizedCard(message);
                break;

            case Constants.TOKE_REFRESH_MESSAGE:
                cardId = intent.getStringExtra(Constants.CARD_ID);
                appUtils.updateToken(message);
                break;

            case Constants.OVER_DUE_TOKE_REFRESH_MESSAGE:
                cardId = intent.getStringExtra(Constants.CARD_ID);
                appUtils.updateBadTokenizedCard(cardId);
                break;

            default:
                //Log to entries
                break;
        }

    }


}
