package xente.business.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import xente.business.interfaces.Flutterwave;
import xente.business.models.CardModel;
import xente.business.processors.ServiceGateway;
import xente.business.utils.Constants;

/**
 * Created by macosx on 22/02/2018.
 */

public class TokenizationService extends Service {

    private CardModel cardModel;

    private IBinder iBinder = new TokenizationBinder();

    @Override
    public IBinder onBind(Intent intent) {
        return iBinder;
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        cardModel = (CardModel)intent.getSerializableExtra(Constants.CARD_MODEL);



        return Service.START_NOT_STICKY;
    }

    public class TokenizationBinder extends Binder {
        public TokenizationService getService() {
            return TokenizationService.this;
        }
    }
}
