package xente.business.utils;

import org.json.JSONException;
import org.json.JSONObject;

import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.FLWChargeResponse;
import xente.business.models.FLWVerifyResponse;

/**
 * Created by macosx on 23/02/2018.
 */

public class Helper {

    public static FLWChargeResponse processFWLChargeRes(String s){

        FLWChargeResponse flwChargeResponse = new FLWChargeResponse();
        try {
            JSONObject obj = new JSONObject(s);

            String status = obj.getString("status");

            JSONObject data = obj.getJSONObject("data");
            String txRef = data.getString("txRef");
            String flwRef = data.getString("flwRef");
            String amount = data.getString("amount");
            String chargeAmount = data.getString("charged_amount");
            String authModelUsed = data.getString("authModelUsed");
            String authUrl = data.getString("authurl");

            JSONObject customerObj = data.getJSONObject("customer");
            String phone = customerObj.getString("phone");
            String fullName = customerObj.getString("fullName");
            String email = customerObj.getString("email");

            flwChargeResponse.setAmount(amount);
            flwChargeResponse.setStatus(status);
            flwChargeResponse.setTxRef(txRef);
            flwChargeResponse.setFlwRef(flwRef);
            flwChargeResponse.setChargeAmount(chargeAmount);
            flwChargeResponse.setPhoneNumber(phone);
            flwChargeResponse.setCustomerName(fullName);
            flwChargeResponse.setEmail(email);
            flwChargeResponse.setAuthModelUsed(authModelUsed);
            flwChargeResponse.setAuthUrl(authUrl);


            return flwChargeResponse;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    public static FLWVerifyResponse processFLWVerifyRes(String s){

        try {
            JSONObject obj = new JSONObject(s);
            String status = obj.getString("status");

            JSONObject dataObj = obj.getJSONObject("data");
            String txRef = dataObj.getString("tx_ref");
            String flw_ref = dataObj.getString("flw_ref");
            String transactionCurrency = dataObj.getString("transaction_currency");
            String amount = dataObj.getString("amount");

            JSONObject flwMetaObj = dataObj.getJSONObject("flwMeta");
            String chargeResponse = flwMetaObj.getString("chargeResponse");

            FLWVerifyResponse flwVerifyResponse = new FLWVerifyResponse();
            flwVerifyResponse.setAmount(amount);
            flwVerifyResponse.setTxRef(txRef);
            flwVerifyResponse.setFlwRef(flw_ref);
            flwVerifyResponse.setTransactionCurrency(transactionCurrency);
            flwVerifyResponse.setChargeResponse(chargeResponse);

            return flwVerifyResponse;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static boolean validateFLWTransactionAndSaveCard(FLWVerifyResponse flwVerifyResponse, CartInstance cartInstance){

        if(!flwVerifyResponse.getFlwRef().equals(cartInstance.getFlwRef())){
            return false;
        }else{
            if(!flwVerifyResponse.getTxRef().equals(cartInstance.getRequestId())){
                return false;
            }else{
                if(!flwVerifyResponse.getTransactionCurrency().equals(cartInstance.getCurrency())){
                    return false;
                }else{
                    if(Integer.valueOf(flwVerifyResponse.getAmount())<Integer.valueOf(cartInstance.getAmount().toPlainString())){
                        return false;
                    }
                    else{
                        if(!flwVerifyResponse.getChargeResponse().equals("00") || !flwVerifyResponse.getChargeResponse().equals("0")){
                            return false;
                        }else{

                            //Save card
                            return true;
                        }
                    }
                }
            }
        }
    }
}
