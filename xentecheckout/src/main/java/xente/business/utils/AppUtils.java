package xente.business.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.TooltipCompat;
import android.util.Log;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import it.sephiroth.android.library.tooltip.Tooltip;
import xente.business.models.MobileNumber;
import xente.business.models.SavedPayment;
import xente.business.xentecheckout.R;

/**
 * Created by macosx on 22/02/2018.
 */

public class AppUtils {

    private Context context;
    private String storedCards;
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    public AppUtils(){

    }

    public AppUtils(Context context){
        this.context = context;
        this.pref = context.getSharedPreferences(Constants.FAT_BOY, Activity.MODE_PRIVATE);
        storedCards = pref.getString(Constants.STORED_NAMES, null);
        this.editor = pref.edit();
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }


    public void saveTokenizedCard(String message){

        try {
            JSONObject card = new JSONObject(message);

            JSONArray cards;
            if(storedCards != null){
                cards = new JSONArray(storedCards);
                cards.put(card);
            }else{
                cards = new JSONArray();
                cards.put(card);
            }

            editor.putString(Constants.STORED_NAMES, cards.toString());
            editor.apply();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void updateToken(String message){
        JSONObject object;
        JSONArray cards;

        Log.d("flutterwave", "updating card");

        if(storedCards != null){

            try {

                object = new JSONObject(message);
                cards = new JSONArray(storedCards);
                JSONArray updatedArray = new JSONArray();

                for(int i=0; i<cards.length(); i++){

                    JSONObject obj = cards.getJSONObject(i);

                    if(obj.getString("card_id").equals(object.getString("card_id"))){
                        Log.d("flutterwave", "old token:"+obj.getString("embedtoken"));
                        Log.d("flutterwave", "new token:"+object.getString("token"));
                        obj.put("embedtoken", object.getString("token"));
                    }

                    updatedArray.put(obj);

                }

                editor.putString(Constants.STORED_NAMES, updatedArray.toString());
                editor.apply();

            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("flutterwave", "error on update:"+e.getMessage());
            }

        }
    }


    public void updateBadTokenizedCard(String cardId){

        if(storedCards != null){

            JSONArray cards;

            if(storedCards != null){

                try {

                    cards = new JSONArray(storedCards);
                    JSONArray updatedArray = new JSONArray();

                    for(int i=0; i<cards.length(); i++){

                        JSONObject obj = cards.getJSONObject(i);

                        if(obj.getString("card_id").equals(cardId)){
                            Log.d("flutterwave", "old token:"+obj.getString("embedtoken"));
                            obj.put("validity", false);
                        }

                        updatedArray.put(obj);

                    }

                    editor.putString(Constants.STORED_NAMES, updatedArray.toString());
                    editor.apply();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("flutterwave", "error on update:"+e.getMessage());
                }

            }

        }

    }

    public void deleteSavedCard(String cardId){
        if(storedCards != null){
            //TODO
        }
    }

    public void saveCardMobileNumber(MobileNumber mobileNumber){


        JSONObject object = new JSONObject();
        try {

            if(!numberExists(mobileNumber)){
                object.put("phone", mobileNumber.getPhoneNumber());
                object.put("full_name", mobileNumber.getAccountName());
                object.put("email", "");
                object.put("expiryyear", "");
                object.put("expirymonth", "");
                object.put("cardBIN", "");
                object.put("last4digits", "");
                object.put("brand", mobileNumber.getProvider());
                object.put("embedtoken", "");
                object.put("payment_provider", mobileNumber.getPaymentProvider());
                object.put("card_id", UUID.randomUUID().toString());
                object.put("validity", true);
                object.put("bank_card", false);

                JSONArray cards;
                if(storedCards != null){
                    cards = new JSONArray(storedCards);
                    cards.put(object);
                }else{
                    cards = new JSONArray();
                    cards.put(object);
                }

                editor.putString(Constants.STORED_NAMES, cards.toString());
                editor.apply();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllCards(){
        editor.clear();
        editor.apply();
    }

    private boolean numberExists(MobileNumber mobileNumber){

        JSONArray cards;
        if(storedCards != null){
            try {
                cards = new JSONArray(storedCards);
                for(int i=0; i<cards.length(); i++){
                    JSONObject cardObj = cards.getJSONObject(i);
                    if(cardObj.getString("phone").equals(mobileNumber.getPhoneNumber())){
                        return true;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static void showToolTip(Context context, View view, String message){


        Tooltip.make(context,
                new Tooltip.Builder(101)
                        .anchor(view, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 3000)
                        .activateDelay(800)
                        .showDelay(800)
                        .text(message)
                        .maxWidth(500)
                        .withArrow(true)
                        .withOverlay(false)
                        .withStyleId(R.style.TooltipLayoutNewStyle)

                        //.floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                        .build()
        ).show();


    }

}
