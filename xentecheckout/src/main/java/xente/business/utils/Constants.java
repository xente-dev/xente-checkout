package xente.business.utils;

/**
 * Created by Intel World on 20/02/2018.
 */
public class Constants {

    //FLW TEST
    public static String FLW_TEST_CHARGE_URL;
    public static String FLW_TEXT_VALIDATE_CHARGE_OPT;
    public static String FLW_SERVICE_URL;
    public static String CYBER_URL;
    public static String CYBER_URL_TEST;


    //Static Block
    static{
        FLW_TEXT_VALIDATE_CHARGE_OPT = "http://flw-pms-dev.eu-west-1.elasticbeanstalk.com/flwv3-pug/getpaidx/api/validatecharge";
        FLW_SERVICE_URL = "https://7033f201.ngrok.io/FluterwaveService/rest/service/payment";
        FLW_TEST_CHARGE_URL = "http://flw-pms-dev.eu-west-1.elasticbeanstalk.com/flwv3-pug/getpaidx/api/charge";

        CYBER_URL =  "https://secureacceptance.cybersource.com/silent/pay";
        CYBER_URL_TEST =  "https://testsecureacceptance.cybersource.com/silent/pay";
    }

    //APP CONSTANTS
    public static String CARD_MODEL = "card_model";
    public static String CART_INSTANCE = "card_instance";
    public static String ERROR_MODEL = "error_model";
    public static String MOBILE_MODEL = "mobile_number";
    public static String CASHBACK_MODEL = "cashback_model";

    public static String CURRENCY = "currency";
    public static String AMOUNT = "amount";
    public static String EMAIL = "email";
    public static String PHONE_NUMBER = "phone_number";
    public static String FIRST_NAME = "first_name";
    public static String LAST_NAME = "last_name";
    public static String TX_REF = "txref";
    public static String COUNTRY = "country";
    public static String PAYMENT_PROVIDER = "payment_provider";
    public static String TOPIC = "topic";
    public static String MQTT_MESSAGE = "mqtt_message";
    public static String FAT_BOY = "fat_boy";
    public static String STORED_NAMES = "stored_names";
    public static String TX_PROCEDURE = "tx_procedure";
    public static String CARD_ID = "card_id";
    public static String REQUEST_ID = "request_id";
    public static String ACCOUNT_NAME = "account_name";

    //NOTIFICATION CONSTANTS
    public static final String NOTIFICATION_PREF = "notification_pref";
    public static final String NOTIFICATION_STRING = "notification_string";
    public static final String NOTIFICATION_STATUS = "status";
    public static final String NOTIFICATION_MESSAGE_STORE = "notification_message_store";

    //PAYMENT PROVIDERS
    public static final String FLUTTERWAVE = "FLUTTERWAVE";
    public static final String CYBERSOURCE = "CYBERSOURCE";
    public static final String STRIPE = "STRIPE";

    //FLUTTERWAVE AUTH MODELS
    public static final String AUTH_BVSECURECODE = "BVSECURECODE";
    public static final String AUTH_PIN = "PIN";

    //FLUTTERWAVE PROCEDURE
    public static final int CAN_CALL_FLUTTERWAVE_SERVICE = 1;
    public static final int CAN_CALL_FLUTTERWAVE_CHARGE = 2;
    public static final int CAN_CALL_FLUTTERWAVE_REDIRECT = 3;
    public static final int CAN_CALL_FLUTTERWAVE_SERVICE_TOKENIZED = 4;
    public static final int CAN_CALL_CYBERSOURCE = 5;



    //CARDS
    public static final String VISA = "VISA";


    //MQTT MESSAGE TYPES
    public static final int TOKENIZATION_MESSAGE = 1;
    public static final int TOKE_REFRESH_MESSAGE = 2;
    public static final int IPN_MESSAGE = 3;
    public static final int OVER_DUE_TOKENIZATION_MESSAGE = 4;
    public static final int OVER_DUE_TOKE_REFRESH_MESSAGE = 5;
    public static final int NOTIFICATION_MESSAGE = 6;
    public static final int OVER_DUE_NOTIFICATION_MESSAGE = 7;

    public static final String MESSAGE_TYPE = "message_type";

}
