package xente.business.utils;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.regex.Pattern;

import xente.business.xentecheckout.BuildConfig;
import xente.business.xentecheckout.R;

/**
 * Created by Intel World on 19/02/2018.
 */
public class CardUtils {

    private FirebaseRemoteConfig firebaseRemoteConfig;
    private String REMOTE_KEY = "payment_source_provider";
    private String paymentProvider;
    private JSONArray providerArray;
    private JSONObject[] providers;
    JSONObject providerObject;

    public CardUtils(FirebaseRemoteConfig firebaseRemoteConfig){

        paymentProvider = firebaseRemoteConfig.getString(REMOTE_KEY);
        Log.d("flutterwave", "Payment provider:"+paymentProvider);

        try {
            providerArray = new JSONArray(paymentProvider);
            providers = new JSONObject[providerArray.length()];
            for(int i=0; i<providerArray.length(); i++){
                providerObject = providerArray.getJSONObject(i);
                providers[i] = providerObject;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public enum CardType {
        UNKNOWN_CARD, AMEX_CARD, MASTER_CARD, VISA_CARD, MOBILE_MONEY_MTN, MOBILE_MONEY_AIRTEL
    }

    public enum MobileNumberProvider{
        MTN, AIRTEL
    }

    private static final Pattern mtnPattern = java.util.regex.Pattern.compile("^(2567|07|2563|03)[7|8|](\\d{7})");
    private static final Pattern airtelPattern = java.util.regex.Pattern.compile("^(2567|07|2562|02)[0|5|](\\d{7})");

    private static final String PATTERN_AMEX = "^3(4|7)[0-9 ]*";
    private static final String PATTERN_VISA = "^4[0-9 ]*";
    private static final String PATTERN_MASTER = "^5[0-9 ]*";
    private static final String PATTERN_MOBILE_MONEY = "^(07|03)[7-8]";

    public static final int MAX_LENGTH_CARD_NUMBER = 16;
    public static final int MAX_LENGTH_CARD_NUMBER_AMEX = 15;

    public static final String CARD_NUMBER_FORMAT = "XXXX XXXX XXXX XXXX";
    public static final String MOBILE_NUMBER_FORMAT = "XXXX XXX XXX";
    public static final String CARD_NUMBER_FORMAT_AMEX = "XXXX XXXXXX XXXXX";
    public static final char CHAR_X = 'X';

    public static final String SPACE_SEPERATOR = " ";
    public static final String SLASH_SEPERATOR = "/";

    public static final int CVV_LENGHT_DEFAULT = 3;
    public static final int CVV_LENGHT_AMEX = 4;
    public static final int CARD_SIDE_FRONT = 1,CARD_SIDE_BACK=0;


    public static String formatCardNumber(String inputCardNumber, String seperator) {
        String unformattedText = inputCardNumber.replace(seperator, "");
        CardType cardType = selectCardType(inputCardNumber);
        String format = (cardType == CardType.AMEX_CARD) ? CARD_NUMBER_FORMAT_AMEX :(cardType == CardType.MOBILE_MONEY_MTN || cardType == CardType.MOBILE_MONEY_AIRTEL)? MOBILE_NUMBER_FORMAT: CARD_NUMBER_FORMAT;
        StringBuilder sbFormattedNumber = new StringBuilder();
        for(int iIdx = 0, jIdx = 0; iIdx < format.length(); iIdx++) {
            if((format.charAt(iIdx) == CHAR_X) && (unformattedText.length() > jIdx))
                sbFormattedNumber.append(unformattedText.charAt(jIdx++));
            else
                sbFormattedNumber.append(format.charAt(iIdx));
        }

        return sbFormattedNumber.toString().replace(SPACE_SEPERATOR, SPACE_SEPERATOR + SPACE_SEPERATOR);
    }

    public static String handleCardNumber(String inputCardNumber) {

        return handleCardNumber(inputCardNumber,SPACE_SEPERATOR);
    }

    public static String handleCardNumber(String inputCardNumber, String seperator) {
        String unformattedText = inputCardNumber.replace(seperator, "");
        CardType cardType = selectCardType(inputCardNumber);

        String format = (cardType == CardType.AMEX_CARD) ? CARD_NUMBER_FORMAT_AMEX : (cardType == CardType.MOBILE_MONEY_MTN || cardType == CardType.MOBILE_MONEY_AIRTEL) ? MOBILE_NUMBER_FORMAT : CARD_NUMBER_FORMAT;
        StringBuilder sbFormattedNumber = new StringBuilder();
        for(int iIdx = 0, jIdx = 0; (iIdx < format.length()) && (unformattedText.length() > jIdx); iIdx++) {
            if(format.charAt(iIdx) == CHAR_X)
                sbFormattedNumber.append(unformattedText.charAt(jIdx++));
            else
                sbFormattedNumber.append(format.charAt(iIdx));
        }

        return sbFormattedNumber.toString();
    }

    public static CardType selectCardType(String cardNumber) {

        if(cardNumber.startsWith("078") || cardNumber.startsWith("077") || cardNumber.startsWith("039"))
            return CardType.MOBILE_MONEY_MTN;

        if(cardNumber.startsWith("070") || cardNumber.startsWith("075"))
            return CardType.MOBILE_MONEY_AIRTEL;

        Pattern pCardType = Pattern.compile(PATTERN_VISA);
        if(pCardType.matcher(cardNumber).matches())
            return CardType.VISA_CARD;
        pCardType = Pattern.compile(PATTERN_MASTER);
        if(pCardType.matcher(cardNumber).matches())
            return CardType.MASTER_CARD;
        pCardType = Pattern.compile(PATTERN_AMEX);
        if(pCardType.matcher(cardNumber).matches())
            return CardType.AMEX_CARD;

        return CardType.UNKNOWN_CARD;



    }



    public static int selectCardLength(CardType cardType) {
        if(cardType == CardType.AMEX_CARD){
            return MAX_LENGTH_CARD_NUMBER_AMEX;
        }
        else{
            return MAX_LENGTH_CARD_NUMBER;
        }

    }

    public static boolean matchMobileNumber(String number){
        if(mtnPattern.matcher(number).matches()){
            return true;
        }
        else if(airtelPattern.matcher(number).matches()){
            return true;
        }
        return false;
    }

    public JSONObject selectedMobileNumberProvider(String number){


        for(JSONObject object : providers){
            Pattern pattern = null;

            try {
                pattern = Pattern.compile(object.getString("regex_start"));
                if(pattern.matcher(number).matches()){
                    return object;
                }

            } catch (JSONException e) {
                return null;
            }

        }
       return null;
    }


    public static String handleExpiration(String month, String year) {

        return handleExpiration(month+year);
    }


    public static String handleExpiration(@NonNull String dateYear) {

        String expiryString = dateYear.replace(SLASH_SEPERATOR, "");

        String text;
        if(expiryString.length() >= 2) {
            String mm = expiryString.substring(0, 2);
            String yy;
            text = mm;

            try {
                if (Integer.parseInt(mm) > 12) {
                    mm = "12"; // Cannot be more than 12.
                }
            }
            catch (Exception e) {
                mm = "01";
            }

            if(expiryString.length() >=4) {
                yy = expiryString.substring(2,4);

                try{
                    Integer.parseInt(yy);
                }catch (Exception e) {

                    Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    yy = String.valueOf(year).substring(2);
                }

                text = mm + SLASH_SEPERATOR + yy;

            }
            else if(expiryString.length() > 2){
                yy = expiryString.substring(2);
                text = mm + SLASH_SEPERATOR + yy;
            }


        }
        else {
            text = expiryString;

        }

        return text;
    }
}
