package xente.business.utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import xente.business.models.FLWChargeResponse;

/**
 * Created by macosx on 09/03/2018.
 */

public class TXProcedure implements Serializable{


    private BitSet bitSet;

    public BitSet getBitSet() {
        return bitSet;
    }

    public void setBitSet(BitSet bitSet) {

        this.bitSet = bitSet;
    }
}
