package xente.business.utils;




import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.cybersource.flex.sdk.FlexService;
import com.cybersource.flex.sdk.FlexServiceFactory;
import com.cybersource.flex.sdk.authentication.Environment;
import com.cybersource.flex.sdk.authentication.FlexCredentials;
import com.cybersource.flex.sdk.authentication.VisaDeveloperCenterCredentials;
import com.cybersource.flex.sdk.exception.FlexException;
import com.cybersource.flex.sdk.impl.VDPSignatureHelper;
import com.cybersource.flex.sdk.model.FlexPublicKey;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.UnrecoverableEntryException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.DestroyFailedException;
import javax.security.cert.CertificateException;

/**
 * Created by Intel World on 20/02/2018.
 */
public class EncryptorDecryptor {

    private static String toHexStr(byte[] bytes){

        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < bytes.length; i++ ){
            builder.append(String.format("%02x", bytes[i]));
        }

        return builder.toString();
    }

    public static String getKeyRave(String seedKey) {
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            byte[] hashedString = md.digest(seedKey.getBytes("utf-8"));
            byte[] subHashString = toHexStr(Arrays.copyOfRange(hashedString, hashedString.length - 12, hashedString.length)).getBytes("utf-8");
            String subSeedKey = seedKey.replace("FLWSECK-", "");
            subSeedKey = subSeedKey.substring(0, 12);
            byte[] combineArray = new byte[24];
            System.arraycopy(subSeedKey.getBytes(), 0, combineArray, 0, 12);
            System.arraycopy(subHashString, subHashString.length - 12, combineArray, 12, 12);
            return new String(combineArray);
        } catch (NoSuchAlgorithmException ex) {
        } catch (UnsupportedEncodingException ex) {
        }
        return null;
    }

    // This is the encryption function that encrypts your payload by passing the stringified format and your encryption Key.

    public static String encryptData(String message, String _encryptionKey)  {
        try {
            final byte[] digestOfPassword = _encryptionKey.getBytes("utf-8");
            final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

            final SecretKey key = new SecretKeySpec( keyBytes , "DESede");
            final Cipher cipher = Cipher.getInstance("DESede/ECB/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            final byte[] plainTextBytes = message.getBytes("utf-8");
            final byte[] cipherText = cipher.doFinal(plainTextBytes);

            return Base64.encodeToString(cipherText, Base64.NO_WRAP);


        } catch (Exception e) {

            e.printStackTrace();
            return "";
        }
    }

    public static void generateCybersouceKeyId(){
        String apiKey = "f9e8cebecbb93e3894644fd406c425cd";
        String secretKey = "76b3cb33cb6c4740a4f960173360df4144AdnAhDXCcj2e6oX4KTLie534AKSwKGs02a1a2mhgdoPALGNnBkeS6hzX5W2t5yYpqHw087a2d4d6546418aaABuCtKhzRzbXY4WmYLquiHavSMCQcK09820a0b56d118U3fnMEy9TVZfcS29vcL35nuf21kGLk0ff6c3ec864eb98331b6ccd4187c6e0b1cb4de710644c798ad76dbdf88ca64";
        VisaDeveloperCenterCredentials visaDeveloperCenterCredentials = new VisaDeveloperCenterCredentials(Environment.TEST, apiKey, secretKey.toCharArray());

        FlexServiceFactory.FlexServiceConfiguration configuration = FlexServiceFactory.FlexServiceConfiguration.DEBUG;
        FlexService flexService = FlexServiceFactory.createInstance(visaDeveloperCenterCredentials, configuration);
        try {
            FlexPublicKey publicKey = flexService.createKey();
            Log.d("flutterwave",publicKey.getKeyId());
        } catch (FlexException e) {
            e.printStackTrace();
            Log.d("flutterwave1",e.getMessage());
        }catch (Exception e){
            Log.d("flutterwave2",e.getMessage());
        }

    }



}
