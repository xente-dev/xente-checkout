package xente.business.enums;

/**
 * Created by Intel World on 20/02/2018.
 */
public enum HttpMethod {

    POST, DELETE, UPDATE, GET, PATCH
}
