package xente.business.models;

import java.io.Serializable;

/**
 * Created by Intel World on 20/02/2018.
 */
public class CardModel implements Serializable{

    public enum AuthModel{
        PIN , BVN , RANDOM_DEBIT , VBVSECURECODE , NOAUTH
    }

    public enum ValidateOption{
        Voice, SMS
    }

    private String cardNumber;
    private String cardName;
    private String expiryMonth;
    private String expiryYear;
    private String bvn;
    private String merchantId;
    private String cvv;
    private String postal;
    private String country;
    private String email;
    private String phoneNumber;
    private String firstName;
    private String lastName;
    private String IP;
    private String paymentProvider;
    private boolean isAddCard;
    private CardModel.ValidateOption validateOption;
    private CardModel.AuthModel authModel;
    private String cardBrand;
    private String cardId;
    private String city;
    private String address;
    private String state;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public String getPaymentProvider() {
        return paymentProvider;
    }

    public void setPaymentProvider(String paymentProvider) {
        this.paymentProvider = paymentProvider;
    }

    public boolean isAddCard() {
        return isAddCard;
    }

    public void setAddCard(boolean addCard) {
        isAddCard = addCard;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public ValidateOption getValidateOption() {
        return validateOption;
    }

    public void setValidateOption(ValidateOption validateOption) {
        this.validateOption = validateOption;
    }

    public AuthModel getAuthModel() {
        return authModel;
    }

    public void setAuthModel(AuthModel authModel) {
        this.authModel = authModel;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public CardModel setMerchantId(String merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    public String getBvn() {
        return bvn;
    }

    public CardModel setBvn(String bvn) {
        this.bvn = bvn;
        return this;
    }

    public String getExpiryMonth() {
        return expiryMonth;
    }

    public CardModel setExpiryMonth(String expiryMonth) {
        this.expiryMonth = expiryMonth;
        return this;
    }

    public String getExpiryYear() {
        return expiryYear;
    }

    public CardModel setExpiryYear(String expiryYear) {
        this.expiryYear = expiryYear;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public CardModel setCountry(String country) {
        this.country = country;
        return this;
    }



    public String getCardNumber() {
        return cardNumber;
    }

    public CardModel setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public String getCardName() {
        return cardName;
    }

    public CardModel setCardName(String cardName) {
        this.cardName = cardName;
        return this;
    }



    public String getCvv() {
        return cvv;
    }

    public CardModel setCvv(String cvv) {
        this.cvv = cvv;
        return this;
    }

    public String getPostal() {
        return postal;
    }

    public CardModel setPostal(String postal) {
        this.postal = postal;
        return this;
    }
}
