package xente.business.models;

/**
 * Created by macosx on 23/02/2018.
 */

public class FLWVerifyResponse {

    private String status;
    private String flwRef;
    private String txRef;
    private String amount;
    private String transactionCurrency;
    private String chargeResponse;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlwRef() {
        return flwRef;
    }

    public void setFlwRef(String flwRef) {
        this.flwRef = flwRef;
    }

    public String getTxRef() {
        return txRef;
    }

    public void setTxRef(String txRef) {
        this.txRef = txRef;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    public void setTransactionCurrency(String transactionCurrency) {
        this.transactionCurrency = transactionCurrency;
    }

    public String getChargeResponse() {
        return chargeResponse;
    }

    public void setChargeResponse(String chargeResponse) {
        this.chargeResponse = chargeResponse;
    }
}
