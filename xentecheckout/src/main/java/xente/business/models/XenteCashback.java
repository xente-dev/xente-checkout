package xente.business.models;

import java.io.Serializable;

/**
 * Created by macosx on 21/03/2018.
 */

public class XenteCashback implements Serializable {
    private String phoneNumber;
    private String description;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
