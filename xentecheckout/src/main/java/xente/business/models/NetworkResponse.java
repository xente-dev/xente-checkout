package xente.business.models;

import java.io.Serializable;

/**
 * Created by Intel World on 20/02/2018.
 */
public class NetworkResponse implements Serializable {

    private int code;
    private String body;

    public int getCode() {
        return code;
    }

    public NetworkResponse setCode(int code) {
        this.code = code;
        return this;
    }

    public String getBody() {
        return body;
    }

    public NetworkResponse setBody(String body) {
        this.body = body;
        return this;
    }
}
