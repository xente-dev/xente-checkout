package xente.business.network;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xente.business.enums.HttpMethod;
import xente.business.models.NetworkResponse;
import xente.business.processors.ServiceImplementation;

/**
 * Created by Dan on 24/10/2017.
 */
public class NetworkHandler<T>
{

    private ServiceImplementation serviceImplementation;

    public NetworkHandler(T context){
        serviceImplementation = (ServiceImplementation)context;
    }

    public void sendRequest(final String url_, Object entity, String httpMethod, boolean isCybersourceCall) {
        //task.execute(url_, entity, httpMethod, isCybersourceCall);
        new Task().execute(url_, entity, httpMethod, isCybersourceCall);
    }

    private boolean isJson(String s)
    {
        try {
            new JSONObject(s);
            return true;
        } catch (JSONException e) {
            try {
                new JSONArray(s);
                return true;
            } catch (JSONException e1) {
                return false;
            }
        }
    }

    private class Task extends AsyncTask<Object, Void, NetworkResponse>{

        @Override
        protected NetworkResponse doInBackground(Object... params) {



            //Strings

            String url = (String)params[0];
            String entity = null;
            HashMap<String, String> csEntity = null;

            String httpMethod = (String)params[2];
            boolean isCybersourceCall = (boolean)params[3];

            if(isCybersourceCall){
                csEntity = (HashMap<String, String>) params[1];
            }else{
                entity = (String)params[1];
            }

            RequestBody requestBody = null;

            if(isCybersourceCall){

                FormBody.Builder formBody = new FormBody.Builder();
                Iterator it = csEntity.entrySet().iterator();

                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    formBody.add((String)pair.getKey(),(String)pair.getValue());
                    it.remove(); // avoids a ConcurrentModificationException
                }
                requestBody = formBody.build();
            }
            else{
                entity = (String)params[1];
            }

            Log.d("flutterwave", "Entity:"+entity);

            final String NETWORK_TIMEOUT = "Connection timed out, please try again.";
            final String NO_CONNECTION = "Connection failed, make sure you have internet connection and try again.";
            final String GENERIC_ERROR = "Unknown error occurred, please try again later.";


            final NetworkResponse networkResponse = new NetworkResponse();
            okhttp3.OkHttpClient client = new okhttp3.OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .followRedirects(false).readTimeout(60, TimeUnit.SECONDS).build();

            HttpUrl.Builder builder = HttpUrl.parse(url).newBuilder();
            String url_ = builder.build().toString();


            Request request = null;
            if (httpMethod.equals(HttpMethod.POST.name())) {

                if(entity != null){
                    request = new Request.Builder().url(url_).post(RequestBody.create(MediaType.parse("application/json"), entity)).build();
                }else{

                    request = new Request.Builder().url(url_).post(requestBody).build();

                }

            }
            else {
                request = new Request.Builder().url(url_).get().build();
            }



            try {
                Response response = client.newCall(request).execute();

                String body = response.body().string();
                int code = response.code();

                Log.d("flutterwave", "body:"+body+"  code:"+code);

                //success
                if (code == 200 || code == 201) {
                    //Check if response is Json, telecos tend to redirect to home page with status 200
                    if (isJson(body)) {
                        networkResponse.setCode(0);
                        networkResponse.setBody(body);
                    } else {
                        if(isCybersourceCall){
                            networkResponse.setCode(0);
                            networkResponse.setBody(body);

                        }else{

                            networkResponse.setCode(-2);
                            networkResponse.setBody(NO_CONNECTION);

                        }

                    }
                }
                //connection or client errors
                else if (code >= 300 && code <= 400) {
                    networkResponse.setCode(-4);
                    networkResponse.setBody(GENERIC_ERROR);
                }
                //connection and socket timeouts
                else if (code == 408 || code == 508) {
                    networkResponse.setCode(-1);
                    networkResponse.setBody(NETWORK_TIMEOUT);
                }
                //other errors
                else {
                    networkResponse.setCode(-4);
                    networkResponse.setBody(GENERIC_ERROR);
                }

            } catch (IOException e) {
                networkResponse.setCode(-2);
                networkResponse.setBody(NO_CONNECTION);
            }

            return networkResponse;
        }

        @Override
        protected void onPostExecute(NetworkResponse result) {
            super.onPostExecute(result);
            serviceImplementation.triggerCallback(result);
        }
    }

   /* AsyncTask<Object, Void, NetworkResponse> task = new AsyncTask<Object, Void, NetworkResponse>() {

    };
*/
}
