package xente.business.xentecheckout;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Calendar;

import xente.business.fragments.BottomSheetFragment;
import xente.business.fragments.CheckoutFragment;
import xente.business.fragments.ErrorBottomSheetFragment;
import xente.business.fragments.OTPBottomFragment;
import xente.business.fragments.SlidingWebViewFragment;
import xente.business.interfaces.Cybersource;
import xente.business.interfaces.Flutterwave;
import xente.business.interfaces.FlutterwaveService;
import xente.business.interfaces.NetworkConnectionCallback;
import xente.business.messages.RequestGenerator;
import xente.business.models.CardModel;
import xente.business.models.FLWChargeResponse;
import xente.business.models.NetworkResponse;
import xente.business.processors.ServiceGateway;
import xente.business.utils.CardUtils;
import xente.business.utils.Constants;
import xente.business.utils.Helper;
import xente.business.utils.TXProcedure;

public class AddPaymentCard extends AppCompatActivity implements View.OnClickListener, TextWatcher,
        View.OnFocusChangeListener, CompoundButton.OnCheckedChangeListener {

    private Button addPayment;
    private EditText mCardNumber;
    private EditText mCvv;
    private EditText mExpiryDate;
    private TextView addTextView;
    private AppCompatCheckBox appCompatCheckBox;

    private boolean isCardLengthCorrect;
    private boolean isCVVCorrect;
    private boolean isExpiryCorrect;
    private boolean isNameCorrect;
    private boolean isAddCardChecked;
    private String rawCardNumber;

    private String CURRENCY;
    private String AMOUNT;
    private String EMAIL;
    private String PHONE_NUMBER;
    private String FIRST_NAME;
    private String LAST_NAME;
    private String TX_REF;
    private String COUNTRY;
    private String PAYMENT_PROVIDER;
    private String CITY;
    private String ADDRESS;
    private String STATE;
    private String POSTAL;

    private CardModel cardModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setTitle(R.string.add_payment);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (getIntent().getExtras() != null) {

            CardModel mCardMoel = (CardModel)getIntent().getSerializableExtra(Constants.CARD_MODEL);
            EMAIL = mCardMoel.getEmail();
            PHONE_NUMBER = mCardMoel.getPhoneNumber();
            FIRST_NAME = mCardMoel.getFirstName();
            LAST_NAME = mCardMoel.getLastName();
            COUNTRY = mCardMoel.getCountry();
            PAYMENT_PROVIDER = mCardMoel.getPaymentProvider();
            CITY = mCardMoel.getCity();
            ADDRESS = mCardMoel.getAddress();
            STATE = mCardMoel.getState();
            POSTAL = mCardMoel.getPostal();
            isAddCardChecked = true;

            Log.d("flutterwave", ""+PAYMENT_PROVIDER);
        }else{
            //quit activity
        }

        init();

    }

    private void init(){
        addPayment = (Button)findViewById(R.id.add_payment);
        mCardNumber = (EditText)findViewById(R.id.card_number);
        mCvv = (EditText)findViewById(R.id.cvv);
        mExpiryDate = (EditText)findViewById(R.id.expiry_date);
        appCompatCheckBox = (AppCompatCheckBox)findViewById(R.id.add_card_check_box);
        addTextView = (TextView)findViewById(R.id.add_card_text);

        //Tint drawables
        tintDrawables(mCardNumber, R.drawable.ic_credit_card_black_24dp, ContextCompat.getColor(this, R.color.lighter_grey));
        //tintDrawables(mName, R.drawable.name_icon, ContextCompat.getColor(this, R.color.lighter_grey));
        tintDrawables(mCvv, R.drawable.required_icon, ContextCompat.getColor(this, R.color.lighter_grey));
        tintDrawables(mExpiryDate, R.drawable.required_icon, ContextCompat.getColor(this, R.color.lighter_grey));


        addPayment.setOnClickListener(this);
        mCardNumber.addTextChangedListener(this);
        //mName.addTextChangedListener(this);
        mCardNumber.setOnFocusChangeListener(this);
        mCvv.addTextChangedListener(this);
        mExpiryDate.addTextChangedListener(this);
        appCompatCheckBox.setOnCheckedChangeListener(this);

    }

    private void tintDrawables(EditText editText, int mDrawable, int color){
        Drawable drawable = getResources().getDrawable(mDrawable);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        editText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
    }

    private Drawable tintDrawables(int mDrawable, int color){
        Drawable drawable = getResources().getDrawable(mDrawable);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.add_payment){

            if(!isCardLengthCorrect){

                setBackgroundAndKeepPadding(mCardNumber, getResources().getDrawable(R.drawable.edit_text_background_error));
            }
            else{

                if(!isCVVCorrect){

                    setBackgroundAndKeepPadding(mCvv, getResources().getDrawable(R.drawable.edit_text_background_error));

                }
                else{
                    if(!isExpiryCorrect){

                        setBackgroundAndKeepPadding(mExpiryDate, getResources().getDrawable(R.drawable.edit_text_background_error));

                    }else{

                        cardModel = new CardModel();

                        cardModel.setAuthModel(CardModel.AuthModel.PIN);
                        cardModel.setValidateOption(CardModel.ValidateOption.SMS);
                        cardModel.setCardNumber(mCardNumber.getText().toString().replaceAll(CardUtils.SPACE_SEPERATOR, "").trim());
                        cardModel.setCvv(mCvv.getText().toString().trim());;
                        cardModel.setEmail(EMAIL);
                        cardModel.setPhoneNumber(PHONE_NUMBER);
                        cardModel.setCountry(COUNTRY);
                        cardModel.setExpiryMonth(mExpiryDate.getText().toString().split("/")[0]);
                        cardModel.setExpiryYear(mExpiryDate.getText().toString().split("/")[1]);
                        cardModel.setBvn("");
                        cardModel.setAddCard(isAddCardChecked);
                        cardModel.setPaymentProvider(PAYMENT_PROVIDER);
                        cardModel.setCity(CITY);
                        cardModel.setAddress(ADDRESS);
                        cardModel.setState(STATE);
                        cardModel.setPostal(POSTAL);

                        cardModel.setFirstName(FIRST_NAME);
                        cardModel.setLastName(LAST_NAME);


                        Intent intent = new Intent();
                        intent.putExtra(Constants.CARD_MODEL, (Serializable) cardModel);

                        if(PAYMENT_PROVIDER.equals(Constants.FLUTTERWAVE)){
                            intent.putExtra(Constants.TX_PROCEDURE, (Serializable)flwTXProcedure());
                        }
                        else if(PAYMENT_PROVIDER.equals(Constants.CYBERSOURCE)){
                            intent.putExtra(Constants.TX_PROCEDURE, (Serializable)cybersouceTXProcedure());
                        }

                        setResult(RESULT_OK, intent);
                        this.finish();

                    }
                }
            }

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if(mCardNumber.getText().hashCode() == s.hashCode()){

            if(!TextUtils.isEmpty(mCardNumber.getText().toString())){
                setBackgroundAndKeepPadding(mCardNumber, getResources().getDrawable(R.drawable.edit_text_background));
            }
            int cursorPosition = mCardNumber.getSelectionEnd();
            int previousLength = mCardNumber.getText().length();

            String cardNumber = CardUtils.handleCardNumber(s.toString());
            int modifiedLength = cardNumber.length();

            mCardNumber.removeTextChangedListener(this);
            mCardNumber.setText(cardNumber);
            rawCardNumber = cardNumber.replace(CardUtils.SPACE_SEPERATOR, "");

            CardUtils.CardType cardType = CardUtils.selectCardType(rawCardNumber);

            if(cardType == CardUtils.CardType.VISA_CARD){
                mCardNumber.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.visa_icon), null, null, null);
            }else if(cardType == CardUtils.CardType.MASTER_CARD){
                mCardNumber.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.mastercard_icon), null, null, null);
            }
            else if(cardType == CardUtils.CardType.AMEX_CARD){
                mCardNumber.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.amex_icon), null, null, null);
            }
            else{

                mCardNumber.setCompoundDrawablesWithIntrinsicBounds(tintDrawables(R.drawable.ic_credit_card_black_24dp, ContextCompat.getColor(this, R.color.default_text_color)), null, null, null);
            }

            int maxLengthWithSpaces = ((cardType == CardUtils.CardType.AMEX_CARD) ? CardUtils.CARD_NUMBER_FORMAT_AMEX : (cardType == CardUtils.CardType.MOBILE_MONEY_MTN || cardType == CardUtils.CardType.MOBILE_MONEY_AIRTEL)? CardUtils.MOBILE_NUMBER_FORMAT : CardUtils.CARD_NUMBER_FORMAT).length();
            mCardNumber.setSelection(cardNumber.length() > maxLengthWithSpaces ? maxLengthWithSpaces : cardNumber.length());
            mCardNumber.addTextChangedListener(this);

            if (modifiedLength <= previousLength && cursorPosition < modifiedLength) {
                mCardNumber.setSelection(cursorPosition);
            }

            if (rawCardNumber.length() == CardUtils.selectCardLength(cardType)) {
                isCardLengthCorrect = true;
            }else{
                isCardLengthCorrect = false;
            }
        }else if(mCvv.getText().hashCode() == s.hashCode()){

            if(rawCardNumber != null){

                CardUtils.CardType cardType = CardUtils.selectCardType(rawCardNumber);
                int cardLength = 3;
                if(cardType == CardUtils.CardType.AMEX_CARD){
                    cardLength = CardUtils.MAX_LENGTH_CARD_NUMBER_AMEX;
                }

                if (s.length() == cardLength) {
                    tintDrawables(mCvv, R.drawable.required_icon, ContextCompat.getColor(this, R.color.colorAccent));
                    setBackgroundAndKeepPadding(mCvv, getResources().getDrawable(R.drawable.edit_text_background));
                    isCVVCorrect = true;
                }
                else{
                    setBackgroundAndKeepPadding(mCvv, getResources().getDrawable(R.drawable.edit_text_background_error));
                    tintDrawables(mCvv, R.drawable.required_icon, ContextCompat.getColor(this, R.color.default_text_color));
                    isCVVCorrect = false;
                }
            }

        }
        else if(mExpiryDate.getText().hashCode() == s.hashCode()){
            String text = s.toString().replace(CardUtils.SLASH_SEPERATOR, "");

            String month, year="";
            if(text.length() >= 2) {
                month = text.substring(0, 2);

                if(text.length() > 2) {
                    year = text.substring(2);
                }

                if(isCardLengthCorrect) {
                    int mm = Integer.parseInt(month);

                    if (mm <= 0 || mm >= 13) {

                        tintDrawables(mExpiryDate, R.drawable.required_icon, ContextCompat.getColor(this, R.color.default_text_color));
                        setBackgroundAndKeepPadding(mExpiryDate, getResources().getDrawable(R.drawable.edit_text_background_error));
                        return;
                    }

                    if (text.length() >= 4) {

                        int yy = Integer.parseInt(year);

                        final Calendar calendar = Calendar.getInstance();
                        int currentYear = calendar.get(Calendar.YEAR);
                        int currentMonth = calendar.get(Calendar.MONTH) + 1;

                        int millenium = (currentYear / 1000) * 1000;


                        if (yy + millenium < currentYear) {

                            isExpiryCorrect = false;
                            tintDrawables(mExpiryDate, R.drawable.required_icon, ContextCompat.getColor(this, R.color.default_text_color));
                            setBackgroundAndKeepPadding(mExpiryDate, getResources().getDrawable(R.drawable.edit_text_background_error));
                            return;

                        } else if (yy + millenium == currentYear && mm < currentMonth) {


                            isExpiryCorrect = false;
                            tintDrawables(mExpiryDate, R.drawable.required_icon, ContextCompat.getColor(this, R.color.default_text_color));
                            setBackgroundAndKeepPadding(mExpiryDate, getResources().getDrawable(R.drawable.edit_text_background_error));
                            return;
                        }
                    }
                }

            }
            else {
                tintDrawables(mExpiryDate, R.drawable.required_icon, ContextCompat.getColor(this, R.color.default_text_color));
                month = text;
            }

            int previousLength = mExpiryDate.getText().length();
            int cursorPosition = mExpiryDate.getSelectionEnd();

            text = CardUtils.handleExpiration(month,year);

            mExpiryDate.removeTextChangedListener(this);
            mExpiryDate.setText(text);
            mExpiryDate.setSelection(text.length());
            mExpiryDate.addTextChangedListener(this);

            int modifiedLength = text.length();

            if(modifiedLength <= previousLength && cursorPosition < modifiedLength) {
                mExpiryDate.setSelection(cursorPosition);
            }
            if(mExpiryDate.getText().toString().length() == 5) {

                isExpiryCorrect = true;
                tintDrawables(mExpiryDate, R.drawable.required_icon, ContextCompat.getColor(this, R.color.colorAccent));
            }else{

                tintDrawables(mExpiryDate, R.drawable.required_icon, ContextCompat.getColor(this, R.color.default_text_color));
                isExpiryCorrect = false;
            }
        }

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(v.getId() == R.id.card_number){

        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    public static void setBackgroundAndKeepPadding(View view, Drawable backgroundDrawable) {
        Rect drawablePadding = new Rect();
        backgroundDrawable.getPadding(drawablePadding);
        int top = view.getPaddingTop() + drawablePadding.top;
        int left = view.getPaddingLeft() + drawablePadding.left;
        int right = view.getPaddingRight() + drawablePadding.right;
        int bottom = view.getPaddingBottom() + drawablePadding.bottom;

        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(backgroundDrawable);
        } else {
            view.setBackground(backgroundDrawable);
        }
        view.setPadding(left, top, right, bottom);
    }



    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        isAddCardChecked = isChecked;
        if(isChecked){
            addTextView.setTextColor(ContextCompat.getColor(this, R.color.default_text_color));
        }else{
            addTextView.setTextColor(ContextCompat.getColor(this, R.color.gray));
        }
    }

    private TXProcedure flwTXProcedure(){
        TXProcedure procedure = new TXProcedure();

        BitSet bitSet = new BitSet();

        bitSet.set(Constants.CAN_CALL_FLUTTERWAVE_SERVICE);;
        bitSet.set(Constants.CAN_CALL_FLUTTERWAVE_CHARGE);
        bitSet.set(Constants.CAN_CALL_FLUTTERWAVE_REDIRECT);


        procedure.setBitSet(bitSet);
        return procedure;

    }

    private TXProcedure cybersouceTXProcedure(){
        TXProcedure procedure = new TXProcedure();
        BitSet bitSet = new BitSet();
        bitSet.set(Constants.CAN_CALL_CYBERSOURCE);

        procedure.setBitSet(bitSet);
        return procedure;
    }


}
