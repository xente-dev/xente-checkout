package xente.business.xentecheckout;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import java.io.Serializable;

import xente.business.fragments.PaymentListFragment;
import xente.business.fragments.SavedPaymentFragment;
import xente.business.models.CardModel;
import xente.business.models.MobileNumber;
import xente.business.models.SavedPayment;
import xente.business.models.XenteCashback;
import xente.business.utils.Constants;
import xente.business.utils.TXProcedure;
import xente.business.xentecheckout.R;

public class PaymentList extends AppCompatActivity {

    private int REQUEST_CODE = 0;
    private CardModel cardModel;
    private String storedCards;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            //getSupportActionBar().setTitle(getResources().getString(R.string.payment_list_title));
        }

        if(getIntent() != null){
            cardModel = (CardModel)getIntent().getSerializableExtra(Constants.CARD_MODEL);
        }

        ////
        SharedPreferences pref = getSharedPreferences(Constants.FAT_BOY, Activity.MODE_PRIVATE);
        storedCards = pref.getString(Constants.STORED_NAMES, null);

        SavedPaymentFragment savedPaymentFragment = SavedPaymentFragment.getInstance();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.CARD_MODEL, (Serializable)cardModel);
        bundle.putSerializable(Constants.STORED_NAMES, storedCards);

        savedPaymentFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.payment_list_container, savedPaymentFragment).commit();

        /////

       /* PaymentListFragment paymentListFragment = PaymentListFragment.getInstance();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.CARD_MODEL, (Serializable)cardModel);
        paymentListFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().add(R.id.payment_list_container, paymentListFragment).commit();*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_CODE){

                CardModel cardModel = null;
                SavedPayment savedPayment = null;
                MobileNumber mobileNumber = null;
                XenteCashback xenteCashback = null;
                TXProcedure procedure = null;

                try{
                    if((CardModel)data.getSerializableExtra(Constants.CARD_MODEL) instanceof CardModel){
                        cardModel = (CardModel) data.getSerializableExtra(Constants.CARD_MODEL);
                        procedure = (TXProcedure) data.getSerializableExtra(Constants.TX_PROCEDURE);
                    }
                }
                catch (ClassCastException e){
                    if((SavedPayment)data.getSerializableExtra(Constants.CARD_MODEL) instanceof SavedPayment){
                        savedPayment = (SavedPayment)data.getSerializableExtra(Constants.CARD_MODEL);
                        procedure = (TXProcedure) data.getSerializableExtra(Constants.TX_PROCEDURE);
                    }

                }
                if((MobileNumber)data.getSerializableExtra(Constants.MOBILE_MODEL) instanceof MobileNumber){
                    mobileNumber = (MobileNumber)data.getSerializableExtra(Constants.MOBILE_MODEL);
                }
                if((XenteCashback)data.getSerializableExtra(Constants.CASHBACK_MODEL) instanceof XenteCashback){
                    xenteCashback = (XenteCashback)data.getSerializableExtra(Constants.CASHBACK_MODEL);
                }


                Intent intent = new Intent();
                if(cardModel != null){
                    intent.putExtra(Constants.CARD_MODEL, (Serializable)cardModel);
                    intent.putExtra(Constants.TX_PROCEDURE, (Serializable)procedure);
                }
                else if(savedPayment != null){
                    intent.putExtra(Constants.CARD_MODEL, (Serializable)savedPayment);
                    intent.putExtra(Constants.TX_PROCEDURE, (Serializable)procedure);
                }
                else if(mobileNumber != null){
                    intent.putExtra(Constants.MOBILE_MODEL,(Serializable)mobileNumber);
                }
                else if(xenteCashback != null){
                    intent.putExtra(Constants.CASHBACK_MODEL, (Serializable)xenteCashback);
                }

                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }





}
