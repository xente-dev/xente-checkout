package xente.business.xentecheckout;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Pattern;

import xente.business.models.MobileNumber;
import xente.business.utils.CardUtils;
import xente.business.utils.Constants;

public class AddPaymentMobileMoney extends AppCompatActivity implements TextWatcher, View.OnClickListener {

    private EditText mPhoneNumber;
    private Button addPaymentMobile;
    private boolean isPhoneNumberLengthCorrect;
    private String ACCOUNT_NAME;
    private CardUtils cardUtils;
    private JSONObject providerObject;
    private String PAYMENT_PROVIDER;
    private String PROVIDER;

    private FirebaseRemoteConfig firebaseRemoteConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment_mobile_money);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.add_payment));
        }

        if(getIntent() != null){
            ACCOUNT_NAME = getIntent().getStringExtra(Constants.ACCOUNT_NAME);
        }

        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        firebaseRemoteConfig.setConfigSettings(configSettings);
        firebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);


        cardUtils = new CardUtils(firebaseRemoteConfig);
        init();
    }

    private void init(){
        mPhoneNumber = (EditText)findViewById(R.id.phone_number);
        addPaymentMobile = (Button) findViewById(R.id.add_payment_mobile);

        tintDrawables(mPhoneNumber, R.drawable.phone_icon, ContextCompat.getColor(this, R.color.default_text_color));
        mPhoneNumber.addTextChangedListener(this);
        addPaymentMobile.setOnClickListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        if(mPhoneNumber.getText().hashCode() == s.hashCode()){

            if(!TextUtils.isEmpty(mPhoneNumber.getText().toString())){
                setBackgroundAndKeepPadding(mPhoneNumber, getResources().getDrawable(R.drawable.edit_text_background));
            }
            int cursorPosition = mPhoneNumber.getSelectionEnd();
            int previousLength = mPhoneNumber.getText().length();

            mPhoneNumber.removeTextChangedListener(this);

            providerObject = cardUtils.selectedMobileNumberProvider(mPhoneNumber.getText().toString().replaceAll(CardUtils.SPACE_SEPERATOR,""));

            if(providerObject != null){
                try {
                    if(providerObject.getString("provider_name").equals(CardUtils.MobileNumberProvider.MTN.name())){
                        mPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.mtn_logo), null, null, null);
                    }else if(providerObject.getString("provider_name").equals(CardUtils.MobileNumberProvider.AIRTEL.name())){
                        mPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.airtel_logo), null, null, null);
                    }
                    else{

                        mPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(tintDrawables(R.drawable.ic_credit_card_black_24dp, ContextCompat.getColor(this, R.color.default_text_color)), null, null, null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                mPhoneNumber.setCompoundDrawablesWithIntrinsicBounds(tintDrawables(R.drawable.ic_credit_card_black_24dp, ContextCompat.getColor(this, R.color.default_text_color)), null, null, null);
            }



            /*int maxLengthWithSpaces = ((cardType == CardUtils.CardType.AMEX_CARD) ? CardUtils.CARD_NUMBER_FORMAT_AMEX : (cardType == CardUtils.CardType.MOBILE_MONEY_MTN || cardType == CardUtils.CardType.MOBILE_MONEY_AIRTEL)? CardUtils.MOBILE_NUMBER_FORMAT : CardUtils.CARD_NUMBER_FORMAT).length();
            mPhoneNumber.setSelection(cardNumber.length() > maxLengthWithSpaces ? maxLengthWithSpaces : cardNumber.length());


            if (modifiedLength <= previousLength && cursorPosition < modifiedLength) {

            }*/

            Pattern pattern = null;
            if(providerObject != null){

                try {
                    pattern = Pattern.compile(providerObject.getString("regex"));

                    if (pattern.matcher(mPhoneNumber.getText().toString().replace(CardUtils.SPACE_SEPERATOR, "")).matches()) {
                        isPhoneNumberLengthCorrect = true;
                        PAYMENT_PROVIDER = providerObject.getString("provider");
                        PROVIDER = providerObject.getString("provider_name");
                    }else{
                        isPhoneNumberLengthCorrect = false;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }else{
                isPhoneNumberLengthCorrect = false;
            }
            mPhoneNumber.addTextChangedListener(this);
            mPhoneNumber.setSelection(cursorPosition);


        }
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.add_payment_mobile){
            if(isPhoneNumberLengthCorrect){

                MobileNumber mobileNumber = new MobileNumber();
                mobileNumber.setMobileNumber(mPhoneNumber.getText().toString().trim().replaceAll(CardUtils.SPACE_SEPERATOR, ""));
                mobileNumber.setAccountName(ACCOUNT_NAME);
                mobileNumber.setCustomerRef(mPhoneNumber.getText().toString().trim().replaceAll(CardUtils.SPACE_SEPERATOR, ""));
                mobileNumber.setPhoneNumber(mPhoneNumber.getText().toString().trim().replaceAll(CardUtils.SPACE_SEPERATOR, ""));
                mobileNumber.setPaymentProvider(PAYMENT_PROVIDER);
                mobileNumber.setProvider(PROVIDER);

                Intent intent = new Intent();
                intent.putExtra(Constants.MOBILE_MODEL, mobileNumber);
                setResult(RESULT_OK, intent);

                finish();

            }

        }
    }

    private void tintDrawables(EditText editText, int mDrawable, int color){
        Drawable drawable = getResources().getDrawable(mDrawable);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        editText.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressWarnings("deprecation")
    public static void setBackgroundAndKeepPadding(View view, Drawable backgroundDrawable) {
        Rect drawablePadding = new Rect();
        backgroundDrawable.getPadding(drawablePadding);
        int top = view.getPaddingTop() + drawablePadding.top;
        int left = view.getPaddingLeft() + drawablePadding.left;
        int right = view.getPaddingRight() + drawablePadding.right;
        int bottom = view.getPaddingBottom() + drawablePadding.bottom;

        int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable(backgroundDrawable);
        } else {
            view.setBackground(backgroundDrawable);
        }
        view.setPadding(left, top, right, bottom);
    }

    private Drawable tintDrawables(int mDrawable, int color){
        Drawable drawable = getResources().getDrawable(mDrawable);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        return drawable;
    }



}
