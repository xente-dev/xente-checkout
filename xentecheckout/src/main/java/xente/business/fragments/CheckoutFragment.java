package xente.business.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.BitSet;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import xente.business.interfaces.CallIP2Service;
import xente.business.interfaces.Cybersource;
import xente.business.interfaces.Flutterwave;
import xente.business.interfaces.FlutterwaveService;
import xente.business.interfaces.NetworkConnectionCallback;
import xente.business.messages.RequestGenerator;
import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.FLWChargeResponse;
import xente.business.models.MobileNumber;
import xente.business.models.NetworkResponse;
import xente.business.models.SavedPayment;
import xente.business.models.XenteCashback;
import xente.business.processors.ServiceGateway;
import xente.business.services.MqttService;
import xente.business.services.NotificationService;
import xente.business.utils.AppUtils;
import xente.business.utils.Constants;
import xente.business.utils.Helper;
import xente.business.utils.TXProcedure;
import xente.business.xentecheckout.R;

/**
 * Created by macosx on 22/02/2018.
 */

public class CheckoutFragment extends Fragment implements NetworkConnectionCallback, View.OnClickListener {

    private CardModel cardModel;
    private SavedPayment savedPayment;
    private CartInstance cartInstance;
    private MobileNumber mobileNumber;
    private XenteCashback xenteCashback;

    private Button confirmButton;
    private BottomSheetDialogFragment bottomSheetDialogFragment = new BottomSheetFragment();
    private BottomSheetDialogFragment errorBottomSheetDialogFragment = new ErrorBottomSheetFragment();
    private BottomSheetDialogFragment slidingWebViewFragment = new SlidingWebViewFragment();
    private BottomSheetDialogFragment otpBottomFragment = new OTPBottomFragment();
    private BottomSheetBehavior progressBottomSheet;
    private View bottomSheetView;


    private boolean callbackWasTriggered;
    private boolean isValidationProcess = false;
    private boolean isCallingCharge;
    private boolean isCallingService;
    private boolean isBound;
    private boolean isMakingCalls;
    private boolean isBoundToNofService;
    private boolean notificationReceived;
    private boolean notificationServiceStarted;

    private TextView cardNumber;
    private TextView cardName;
    private TextView expiry;
    private TextView processingMsg;
    RelativeLayout frontCardOutline;
    private ImageView logo;
    private MaterialProgressBar paymentProcessingProgress;

    private TXProcedure txProcedure;
    private BitSet txBitSet;
    private String PAYMENT_PROVIDER;
    private String notificationMessage;

    private MqttService mqttService;
    private MqttService.MBinder serviceBinder;
    private NotificationService.NotificationBinder notificationBinder;
    private NotificationService notificationService;
    private Intent intent;
    private Intent notificationIntent;


    private CheckoutCallack checkoutCallback;

    public interface CheckoutCallack{
        void showProgressSheet();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        checkoutCallback = (CheckoutCallack)activity;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            try{
                if((CardModel)getArguments().getSerializable(Constants.CARD_MODEL) instanceof CardModel){
                    cardModel = (CardModel) getArguments().getSerializable(Constants.CARD_MODEL);
                    PAYMENT_PROVIDER = cardModel.getPaymentProvider();

                    txProcedure = (TXProcedure)getArguments().getSerializable(Constants.TX_PROCEDURE);
                    txBitSet = txProcedure.getBitSet();
                    cartInstance = (CartInstance)getArguments().getSerializable(Constants.CART_INSTANCE);
                }

            }catch (ClassCastException e){
                if((SavedPayment)getArguments().getSerializable(Constants.CARD_MODEL) instanceof SavedPayment){
                    savedPayment = (SavedPayment)getArguments().getSerializable(Constants.CARD_MODEL);
                    PAYMENT_PROVIDER = savedPayment.getPaymentProvider();
                    cartInstance = (CartInstance)getArguments().getSerializable(Constants.CART_INSTANCE);

                    if(savedPayment.isBankCard()){
                        txProcedure = (TXProcedure)getArguments().getSerializable(Constants.TX_PROCEDURE);
                        txBitSet = txProcedure.getBitSet();

                    }

                }
            }

            if((MobileNumber)getArguments().getSerializable(Constants.MOBILE_MODEL) instanceof MobileNumber){
                cartInstance = (CartInstance)getArguments().getSerializable(Constants.CART_INSTANCE);
                mobileNumber = (MobileNumber)getArguments().getSerializable(Constants.MOBILE_MODEL);
            }
            if((XenteCashback)getArguments().getSerializable(Constants.CASHBACK_MODEL) instanceof XenteCashback){
                xenteCashback = (XenteCashback)getArguments().getSerializable(Constants.CASHBACK_MODEL);
                cartInstance = (CartInstance)getArguments().getSerializable(Constants.CART_INSTANCE);
            }

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.checkout_fragment_layout, container, false);
        initViews(rootView);
        return rootView;
    }

    private void initViews(View v){


        confirmButton = (Button)v.findViewById(R.id.confirm_button);
        paymentProcessingProgress = (MaterialProgressBar)v.findViewById(R.id.payment_processing_progress);
        processingMsg = (TextView)v.findViewById(R.id.processing_message);

        bottomSheetView = (View)v.findViewById(R.id.bottom_sheet);

        progressBottomSheet = BottomSheetBehavior.from(bottomSheetView);
        progressBottomSheet.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    progressBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                }else if(newState == BottomSheetBehavior.STATE_EXPANDED){

                    progressBottomSheet.setHideable(false);

                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        progressBottomSheet.setHideable(true);
        progressBottomSheet.setState(BottomSheetBehavior.STATE_HIDDEN);

        confirmButton.setOnClickListener(this);

    }



    @Override
    public void callback(NetworkResponse networkResponse) {

        Log.d("flutterwave", "Network callback status:"+networkResponse.getCode());
        if(networkResponse.getCode() != 0){

            if(bottomSheetDialogFragment != null && bottomSheetDialogFragment.isResumed()){
                bottomSheetDialogFragment.dismiss();
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.ERROR_MODEL, (Serializable)networkResponse);

            if(errorBottomSheetDialogFragment != null){

                errorBottomSheetDialogFragment.setArguments(bundle);
                errorBottomSheetDialogFragment.setCancelable(false);
                errorBottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), errorBottomSheetDialogFragment.getTag());
            }
            if(mobileNumber == null){
                resetUI();
            }


            isMakingCalls = false;
        }
        else if(networkResponse.getCode() == 0){
            if(Constants.FLUTTERWAVE.equals(PAYMENT_PROVIDER)){

                if(txBitSet.get(Constants.CAN_CALL_FLUTTERWAVE_CHARGE)){
                    txBitSet.flip(Constants.CAN_CALL_FLUTTERWAVE_CHARGE);
                    invokeFlutterwaveCharge();
                }
                else if(!txBitSet.get(Constants.CAN_CALL_FLUTTERWAVE_CHARGE)){
                    //show auth ui
                    if(!txBitSet.get(Constants.CAN_CALL_FLUTTERWAVE_CHARGE) && txBitSet.get(Constants.CAN_CALL_FLUTTERWAVE_REDIRECT))
                    showFLWAuthFragment(networkResponse.getBody());
                }
                if(!txBitSet.get(Constants.CAN_CALL_FLUTTERWAVE_REDIRECT)){
                    //successful validation
                    cancelProgressAfterSuccess();
                }
                else if(txBitSet.get(Constants.CAN_CALL_FLUTTERWAVE_SERVICE_TOKENIZED)){
                    txBitSet.flip(Constants.CAN_CALL_FLUTTERWAVE_SERVICE_TOKENIZED);
                    cancelProgressAfterSuccess();
                }

            }else if(Constants.CYBERSOURCE.equals(PAYMENT_PROVIDER)){

                if(txBitSet.get(Constants.CAN_CALL_CYBERSOURCE)){
                    txBitSet.flip(Constants.CAN_CALL_CYBERSOURCE);
                    cancelProgressAfterSuccess();
                }

            }else{
                cancelProgressAfterSuccess();
                /*if(mobileNumber != null){
                    new AppUtils(getActivity()).saveCardMobileNumber(mobileNumber);
                }*/
            }

        }
    }

    public void dismissWebView(String url){
        txBitSet.flip(Constants.CAN_CALL_FLUTTERWAVE_REDIRECT);
        if(slidingWebViewFragment != null && slidingWebViewFragment.isVisible()){
            slidingWebViewFragment.dismiss();
        }

        bottomSheetDialogFragment.setCancelable(false);
        bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

        isValidationProcess = true;
        FlutterwaveService flutterwaveService = new ServiceGateway();
        flutterwaveService.postFlutterwaveValidatationToService(url, this);
    }



    public void UIOTPOkActionClicked(boolean clicked, String otp){
        if(!clicked){
            if(otpBottomFragment != null && otpBottomFragment.isVisible()){
                otpBottomFragment.dismiss();
            }
        }
        else{

            if(otpBottomFragment != null && otpBottomFragment.isVisible()){
                otpBottomFragment.dismiss();
            }
            bottomSheetDialogFragment.setCancelable(false);
            bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

            Flutterwave flutterwave = new ServiceGateway();
            flutterwave.flutterwaveValidateCardChargeOTP(cartInstance.getRequestId(), Integer.valueOf(otp), this);
        }
    }

    @Override
    public void onClick(View v) {

        if(cardModel != null){
            processCardPayment(cardModel);
        }else if(savedPayment != null){
            processSavedCardPayment(savedPayment);
        }else if(mobileNumber != null){
            processMobilePayment();

            new AppUtils(getActivity()).saveCardMobileNumber(mobileNumber);
        }else if(xenteCashback != null){
            processCashbackPayment();
        }
    }

    private void processMobilePayment(){
        CallIP2Service callIP2Service = new ServiceGateway();
        callIP2Service.requestIP2Payment(mobileNumber, cartInstance, this);
    }

    private void processCashbackPayment(){
        CallIP2Service callIP2Service = new ServiceGateway();
        callIP2Service.requestIP2Payment(xenteCashback, cartInstance, this);
    }

    private void processCardPayment(CardModel cardModel){


        if(Constants.FLUTTERWAVE.equals(cardModel.getPaymentProvider())){

            isMakingCalls = true;

            if(txBitSet.get(Constants.CAN_CALL_FLUTTERWAVE_SERVICE))
            {
                FlutterwaveService flutterwaveService = new ServiceGateway();

                flutterwaveService.postFlutterwaveTransactionToService(RequestGenerator.flutterWaveServicePayload(cardModel, cartInstance), this);
                bottomSheetDialogFragment.setCancelable(false);
                bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }


        }
        else if(Constants.CYBERSOURCE.equals(cardModel.getPaymentProvider())){


            isMakingCalls = true;
            if(txBitSet.get(Constants.CAN_CALL_CYBERSOURCE)){

                Cybersource cybersource = new ServiceGateway();
                cybersource.requestCybersourceCardPayment(cardModel,  cartInstance, this);

                bottomSheetDialogFragment.setCancelable(false);
                bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        }

    }

    private void processSavedCardPayment(SavedPayment savedPayment){

        if(savedPayment.isBankCard()){
            if(Constants.FLUTTERWAVE.equals(savedPayment.getPaymentProvider())){

                isMakingCalls = true;
                if(txBitSet.get(Constants.CAN_CALL_FLUTTERWAVE_SERVICE_TOKENIZED)){

                    FlutterwaveService flutterwaveService = new ServiceGateway();

                    flutterwaveService.postFlutterwaveTokenizedTransactionToService(RequestGenerator.flutterwaveTokenizedTransationPayload(savedPayment), this);
                    bottomSheetDialogFragment.setCancelable(false);
                    bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                }

            }
            else if(Constants.CYBERSOURCE.equals(cardModel.getPaymentProvider())){

                isMakingCalls = true;
                Cybersource cybersource = new ServiceGateway();
                cybersource.requestCybersourceCardPayment(cardModel, cartInstance, this);

                bottomSheetDialogFragment.setCancelable(false);
                bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
            }
        }else{

            MobileNumber mobileNumber = new MobileNumber();
            mobileNumber.setMobileNumber(savedPayment.getPhone());
            mobileNumber.setAccountName(savedPayment.getFullName());
            mobileNumber.setCustomerRef(savedPayment.getPhone());
            mobileNumber.setPhoneNumber(savedPayment.getPhone());
            mobileNumber.setPaymentProvider(savedPayment.getPaymentProvider());

            CallIP2Service callIP2Service = new ServiceGateway();
            callIP2Service.requestIP2Payment(mobileNumber, cartInstance, this);
        }


    }


    private void invokeFlutterwaveCharge(){
        Flutterwave flutterwave = new ServiceGateway();
        flutterwave.requestFLWCardPayment(cardModel, cartInstance,  this);
    }

    private void showFLWAuthFragment(String s){

        if(bottomSheetDialogFragment!= null && bottomSheetDialogFragment.isVisible()){
            bottomSheetDialogFragment.dismiss();
        }

        FLWChargeResponse flwChargeResponse = Helper.processFWLChargeRes(s);

        if(CardModel.AuthModel.PIN.name().equals(flwChargeResponse.getAuthModelUsed())){

            if(bottomSheetDialogFragment != null && bottomSheetDialogFragment.isResumed()){
                bottomSheetDialogFragment.dismiss();

            }


            Bundle bundle  = new Bundle();
            bundle.putString(Constants.PHONE_NUMBER, cardModel.getPhoneNumber());
            otpBottomFragment.setArguments(bundle);
            otpBottomFragment.setCancelable(false);
            otpBottomFragment.show(getActivity().getSupportFragmentManager(), otpBottomFragment.getTag());

        }else{

            if(bottomSheetDialogFragment != null && bottomSheetDialogFragment.isResumed()){
                bottomSheetDialogFragment.dismiss();

            }
            Bundle bundle = new Bundle();
            bundle.putString("authurl", flwChargeResponse.getAuthUrl());
            slidingWebViewFragment.setArguments(bundle);
            slidingWebViewFragment.setCancelable(false);
            slidingWebViewFragment.show(getActivity().getSupportFragmentManager(), slidingWebViewFragment.getTag());
        }
    }

    private void cancelProgressAfterSuccess(){

        isMakingCalls = false;

        if(bottomSheetDialogFragment != null && bottomSheetDialogFragment.isVisible()){
            bottomSheetDialogFragment.dismiss();
        }

        notificationMessage = getString(R.string.processing_message);
        processingMsg.setText(notificationMessage);

        progressBottomSheet.setState(BottomSheetBehavior.STATE_EXPANDED);

        intent = new Intent(getActivity(), MqttService.class);

        if(cardModel != null){

            if(Constants.FLUTTERWAVE.equals(cardModel.getPaymentProvider())){

                if(cardModel.isAddCard()){

                    intent.putExtra(Constants.TOPIC, cardModel.getPhoneNumber());
                    intent.putExtra(Constants.MESSAGE_TYPE, Constants.TOKENIZATION_MESSAGE);
                    getActivity().startService(intent);
                    getActivity().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
                }

                startNotificationService();
            }else if(Constants.CYBERSOURCE.equals(cardModel.getPaymentProvider())){

                startNotificationService();
            }

        }
        else if(savedPayment != null){
            intent.putExtra(Constants.TOPIC, savedPayment.getPhone());
            intent.putExtra(Constants.MESSAGE_TYPE, Constants.TOKE_REFRESH_MESSAGE);
            intent.putExtra(Constants.CARD_ID, savedPayment.getCardId());
            getActivity().startService(intent);
            getActivity().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);

            startNotificationService();

        }else if(mobileNumber != null){
            startNotificationService();
        }


    }

    private void resetUI(){
        if(cardModel != null){

            if(Constants.FLUTTERWAVE.equals(PAYMENT_PROVIDER)){
                txBitSet.set(Constants.CAN_CALL_FLUTTERWAVE_SERVICE);;
                txBitSet.set(Constants.CAN_CALL_FLUTTERWAVE_CHARGE);
                txBitSet.set(Constants.CAN_CALL_FLUTTERWAVE_REDIRECT);
            }
            else if(Constants.CYBERSOURCE.equals(PAYMENT_PROVIDER)){
                txBitSet.set(Constants.CAN_CALL_CYBERSOURCE);
            }

        }
        else if(savedPayment != null){
            txBitSet.set(Constants.CAN_CALL_FLUTTERWAVE_SERVICE_TOKENIZED);
        }

    }

    private void startNotificationService(){
        notificationReceived = false;
        notificationServiceStarted = true;

        notificationIntent = new Intent(getActivity(), NotificationService.class);
        notificationIntent.putExtra(Constants.MESSAGE_TYPE, Constants.NOTIFICATION_MESSAGE);
        notificationIntent.putExtra(Constants.TOPIC, cardModel.getPhoneNumber());
        notificationIntent.putExtra(Constants.REQUEST_ID, cartInstance.getRequestId());
        getActivity().startService(notificationIntent);
        getActivity().bindService(notificationIntent, notificationConnection, Context.BIND_AUTO_CREATE);
    }


    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            serviceBinder = (MqttService.MBinder) service;
            mqttService = serviceBinder.getService();
            isBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    private ServiceConnection notificationConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            Log.d("flutterwave", "Notification connection acquired");
            notificationBinder = (NotificationService.NotificationBinder)service;
            notificationService = notificationBinder.getService();
            isBoundToNofService = true;
            notificationService.amAlive();
            registerNofReceiver();

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBoundToNofService = false;
            Log.d("flutterwave", "Notification connection denied");

        }
    };

    private BroadcastReceiver notificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("flutterwave", "Broadcast receiver onrecived");
            notificationReceived = true;
            notificationServiceStarted = false;

            paymentProcessingProgress.setIndeterminate(false);
            paymentProcessingProgress.setVisibility(View.INVISIBLE);

            notificationMessage = getString(R.string.processing_message_overdue);
            processingMsg.setText(notificationMessage);
            //show progress sheet
        }
    };


    private void registerNofReceiver(){
        IntentFilter intentFilter = new IntentFilter(NotificationService.BROADCAST_ACTION);
        getActivity().registerReceiver(notificationReceiver, intentFilter);
    }

    @Override
    public void onPause() {


        if (isBound) {
            getActivity().unbindService(serviceConnection);
            isBound = false;
        }
        if(isBoundToNofService){
            notificationService.amDead();
            getActivity().unbindService(notificationConnection);
            isBoundToNofService = false;
            getActivity().unregisterReceiver(notificationReceiver);
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(isMakingCalls && !isBound){
            if(intent != null){
                getActivity().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
            }

        }
        if(!isBoundToNofService && !notificationReceived && notificationServiceStarted){
            getActivity().bindService(notificationIntent, notificationConnection, Context.BIND_AUTO_CREATE);
        }

    }


}
