package xente.business.fragments;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import xente.business.models.NetworkResponse;
import xente.business.xentecheckout.R;

/**
 * Created by macosx on 23/02/2018.
 */

public class SlidingWebViewFragment extends BottomSheetDialogFragment {

    private WebView authWebView;
    private String url;
    private MaterialProgressBar pageLoadProgress;

    private ActivityCallback activityCallback;

    public interface ActivityCallback{
        void cancelWebView(String url);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            url = getArguments().getString("authurl");
        }

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        activityCallback = (ActivityCallback)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.web_view_layout, container, false);

        init(v);
        return v;
    }

    private void init(View v){
        authWebView = (WebView)v.findViewById(R.id.auth_webview);
        pageLoadProgress = (MaterialProgressBar)v.findViewById(R.id.page_load_progress);
        loadAuthURL();

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    private void loadAuthURL(){
        WebSettings webSettings = authWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        authWebView.setWebViewClient(new WebClient());


        authWebView.loadUrl(url);
    }

    private class WebClient extends WebViewClient{
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            if(!url.contains("/FluterwaveService/rest/service/flutterwave/redirect")) {
                view.loadUrl(url);
                return true;

            } else {
               activityCallback.cancelWebView(url);
               return true;
            }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pageLoadProgress.setVisibility(View.GONE);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            pageLoadProgress.setVisibility(View.VISIBLE);
        }
    }
}
