package xente.business.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import xente.business.models.NetworkResponse;
import xente.business.xentecheckout.R;

/**
 * Created by Intel World on 19/02/2018.
 */
public class BottomSheetFragment extends BottomSheetDialogFragment {

    public BottomSheetFragment() {
        // Required empty public constructor
    }

    private ActivityCallback activityCallback;

    public interface ActivityCallback{
        void callback(NetworkResponse networkResponse);
    }


    private TextView errorText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_bottom_sheet_layout, container, false);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

    }


}
