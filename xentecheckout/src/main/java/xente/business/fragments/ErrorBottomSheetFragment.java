package xente.business.fragments;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import xente.business.models.NetworkResponse;
import xente.business.utils.Constants;
import xente.business.xentecheckout.R;

/**
 * Created by Intel World on 19/02/2018.
 */
public class ErrorBottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    public ErrorBottomSheetFragment() {
        // Required empty public constructor
    }

    private TextView errorText;
    private Button okButton;
    private Button cancelButton;
    private NetworkResponse networkResponse;
    private ImageView errorIcon;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            networkResponse = (NetworkResponse)getArguments().getSerializable(Constants.ERROR_MODEL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.error_fragment_bottom_sheet_layout, container, false);
        init(v);
        return v;
    }

    private void init(View v){
        errorText = (TextView)v.findViewById(R.id.error_text);
        okButton = (Button)v.findViewById(R.id.ok_button);
        cancelButton = (Button)v.findViewById(R.id.cancel_button);
        errorIcon = (ImageView)v.findViewById(R.id.error_icon);


        okButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);

        errorIcon.setImageDrawable(tintDrawables(R.drawable.error_icon, ContextCompat.getColor(getActivity(), R.color.default_text_color)));
        String errorT = getString(R.string.error, networkResponse.getBody());
        errorText.setText(errorT);
    }

    private Drawable tintDrawables(int mDrawable, int color){
        Drawable drawable = getResources().getDrawable(mDrawable);
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, color);
        DrawableCompat.setTintMode(drawable, PorterDuff.Mode.SRC_IN);
        return drawable;
    }

    @Override
    public void onClick(View v) {
        this.dismiss();
    }
}
