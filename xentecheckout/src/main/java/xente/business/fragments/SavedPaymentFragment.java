package xente.business.fragments;


import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import xente.business.adapters.SavedPaymentAdapter;
import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.SavedPayment;
import xente.business.utils.AppUtils;
import xente.business.utils.Constants;
import xente.business.utils.TXProcedure;
import xente.business.xentecheckout.AddPaymentCard;
import xente.business.xentecheckout.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by macosx on 06/03/2018.
 */

public class SavedPaymentFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private FloatingActionButton addCard;
    private ListView savedPaymentListView;
    private CardModel cardModel;
    private String storedCards;
    private List<SavedPayment> savedPaymentList;
    private int REQUEST_CODE = 0;
    private SavedPaymentAdapter savedPaymentAdapter;


    private static SavedPaymentFragment savedPaymentFragment;

    public SavedPaymentFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            cardModel = (CardModel)getArguments().getSerializable(Constants.CARD_MODEL);
            storedCards = getArguments().getString(Constants.STORED_NAMES);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.saved_payment_frag_layout, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View v){
        addCard = (FloatingActionButton) v.findViewById(R.id.add_card);
        savedPaymentListView = (ListView)v.findViewById(R.id.saved_payment_list);

        addCard.setOnClickListener(this);


        if(storedCards != null){
            setUpList(storedCards);
        }


        AppUtils.showToolTip(getActivity(), addCard, "Click here to add payments");
    }



    private void setUpList(String s){

        savedPaymentList = new ArrayList<SavedPayment>();

        try {
            JSONArray cards = new JSONArray(s);

            for(int i=0; i<cards.length(); i++){
                JSONObject obj = cards.getJSONObject(i);

                String phone = obj.getString("phone");
                String fullName = obj.getString("full_name");
                String email = obj.getString("email");
                String expiryyear = obj.getString("expiryyear");
                String expirymonth = obj.getString("expirymonth");
                String cardBIN = obj.getString("cardBIN");
                String last4Digits = obj.getString("last4digits");
                String brand = obj.getString("brand");
                String embedToken = obj.getString("embedtoken");
                String paymentProvider = obj.getString("payment_provider");
                String cardId = obj.getString("card_id");
                boolean validity = obj.getBoolean("validity");
                boolean isBankCard = obj.getBoolean("bank_card");

                SavedPayment savedPayment = new SavedPayment();
                savedPayment.setBrand(brand);
                savedPayment.setPhone(phone);
                savedPayment.setFullName(fullName);
                savedPayment.setEmail(email);
                savedPayment.setExpiryyear(expiryyear);
                savedPayment.setExpirymonth(expirymonth);
                savedPayment.setCardBIN(cardBIN);
                savedPayment.setLast4digits(last4Digits);
                savedPayment.setEmbedToken(embedToken);
                savedPayment.setPaymentProvider(paymentProvider);
                savedPayment.setCardId(cardId);
                savedPayment.setValidity(validity);
                savedPayment.setBankCard(isBankCard);

                savedPaymentList.add(savedPayment);
            }

            savedPaymentAdapter = new SavedPaymentAdapter(savedPaymentList, getActivity());
            savedPaymentListView.setAdapter(savedPaymentAdapter);
            savedPaymentListView.setOnItemClickListener(this);

        } catch (JSONException e) {
            //Delete all cards, the json is no longer a valid one
           new AppUtils(getActivity()).deleteAllCards();
        }


    }

    public static SavedPaymentFragment getInstance(){
        if(savedPaymentFragment == null){
            savedPaymentFragment = new SavedPaymentFragment();
        }
        return savedPaymentFragment;
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.add_card){

            PaymentListFragment paymentListFragment = PaymentListFragment.getInstance();
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.CARD_MODEL, (Serializable)cardModel);
            paymentListFragment.setArguments(bundle);
            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.payment_list_container, paymentListFragment).addToBackStack(null).commit();
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(!savedPaymentList.get(position).isValid()){

            showInvalidCardMessage(savedPaymentList.get(position).getCardId());

        }else{

            Intent intent = new Intent();
            intent.putExtra(Constants.CARD_MODEL, (Serializable)savedPaymentList.get(position));

            if(Constants.FLUTTERWAVE.equals(savedPaymentList.get(position).getPaymentProvider())){
                intent.putExtra(Constants.TX_PROCEDURE, (Serializable)flwTXProcedure());
            }

            else if(Constants.CYBERSOURCE.equals(savedPaymentList.get(position).getPaymentProvider())){
                intent.putExtra(Constants.TX_PROCEDURE, (Serializable)cybersouceTXProcedure());
            }

            getActivity().setResult(RESULT_OK, intent);
            getActivity().finish();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("flutterwave", "result was called");
        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_CODE){
                CardModel cardModel = (CardModel) data.getSerializableExtra(Constants.CARD_MODEL);
                TXProcedure procedure = (TXProcedure) data.getSerializableExtra(Constants.TX_PROCEDURE);
                Intent intent = new Intent();
                intent.putExtra(Constants.CARD_MODEL, (Serializable)cardModel);
                intent.putExtra(Constants.TX_PROCEDURE,(Serializable)procedure);
                getActivity().setResult(RESULT_OK, intent);
                getActivity().finish();
            }
        }
    }

    private TXProcedure flwTXProcedure(){
        TXProcedure procedure = new TXProcedure();

        BitSet bitSet = new BitSet();

        bitSet.set(Constants.CAN_CALL_FLUTTERWAVE_SERVICE_TOKENIZED);

        procedure.setBitSet(bitSet);
        return procedure;

    }

    private TXProcedure cybersouceTXProcedure(){
        TXProcedure procedure = new TXProcedure();
        BitSet bitSet = new BitSet();
        bitSet.set(Constants.CAN_CALL_CYBERSOURCE);

        procedure.setBitSet(bitSet);
        return procedure;
    }

    public void showInvalidCardMessage(final String cardId){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.delete_card_msg));
        builder.setPositiveButton(getString(R.string._remove), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                removeCard(cardId);
            }
        });

        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void removeCard(String cardId){
        for(SavedPayment savedPayment : savedPaymentList){
            if(cardId.equals(savedPayment.getCardId())){
                savedPaymentList.remove(savedPayment);
                break;
            }
        }

        savedPaymentAdapter = new SavedPaymentAdapter(savedPaymentList, getActivity());
        savedPaymentListView.setAdapter(savedPaymentAdapter);
        savedPaymentAdapter.notifyDataSetChanged();

        deleteSavedCardFromPref(cardId);
    }

    private void deleteSavedCardFromPref(String cardId){
        AppUtils appUtils = new AppUtils(getActivity());
        appUtils.deleteSavedCard(cardId);
    }
}
