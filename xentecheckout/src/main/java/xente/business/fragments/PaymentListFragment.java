package xente.business.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import xente.business.models.CardModel;
import xente.business.models.MobileNumber;
import xente.business.models.SavedPayment;
import xente.business.models.XenteCashback;
import xente.business.utils.CardUtils;
import xente.business.utils.Constants;
import xente.business.utils.TXProcedure;
import xente.business.xentecheckout.AddPaymentCard;
import xente.business.xentecheckout.AddPaymentMobileMoney;
import xente.business.xentecheckout.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Intel World on 15/02/2018.
 */
public class PaymentListFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    public static PaymentListFragment paymentListFragment;

    private int REQUEST_CODE = 0;
    public PaymentListFragment(){
    }


    private int[] icons;
    private String[] paymentTypes;
    private ListView paymentMethodList;
    private String storedCards;

    private CardModel cardModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            cardModel = (CardModel)getArguments().getSerializable(Constants.CARD_MODEL);
        }

        SharedPreferences pref = getActivity().getSharedPreferences(Constants.FAT_BOY, Activity.MODE_PRIVATE);
        storedCards = pref.getString(Constants.STORED_NAMES, null);


        icons = new int[]{R.drawable.ic_credit_card_black_24dp, R.drawable.ic_credit_card_black_24dp, R.drawable.ic_credit_card_black_24dp};
        paymentTypes = new String[]{getString(R.string.credit_debit_card), getString(R.string.mobile_money_), getString(R.string.xente_cashback)};
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getResources().getString(R.string.payment_list_title));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.payment_list_frag_layout, container, false);
        init(rootView);
        return rootView;
    }

    private void init(View v){

        paymentMethodList = (ListView)v.findViewById(R.id.payment_list_view);
        paymentMethodList.setOnItemClickListener(this);

        List<HashMap<String, String>> aList = new ArrayList<HashMap<String, String>>();

        for(int i=0; i<paymentTypes.length; i++){
            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("payment_name", paymentTypes[i]);
            hashMap.put("payment_icon", Integer.toString(icons[i]));

            aList.add(hashMap);
        }

        String[] from = {"payment_name", "payment_icon"};
        int [] to = {R.id.payment_item_name, R.id.payment_item_icon};

        SimpleAdapter simpleAdapter = new SimpleAdapter(getActivity(), aList, R.layout.payment_list_item, from, to);
        paymentMethodList.setAdapter(simpleAdapter);


    }

    public static PaymentListFragment getInstance(){
        if(paymentListFragment == null){
            paymentListFragment = new PaymentListFragment();
        }
        return paymentListFragment;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:

                Intent intentCard = new Intent(getActivity(), AddPaymentCard.class);
                intentCard.putExtra(Constants.CARD_MODEL, (Serializable)cardModel);
                getActivity().startActivityForResult(intentCard, REQUEST_CODE);
                /*if(storedCards == null){
                    Intent intent = new Intent(getActivity(), AddPaymentCard.class);
                    intent.putExtra(Constants.CARD_MODEL, (Serializable)cardModel);
                    startActivityForResult(intent, REQUEST_CODE);
                }else{

                    SavedPaymentFragment savedPaymentFragment = SavedPaymentFragment.getInstance();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Constants.CARD_MODEL, (Serializable)cardModel);
                    bundle.putSerializable(Constants.STORED_NAMES, storedCards);

                    savedPaymentFragment.setArguments(bundle);

                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.payment_list_container, savedPaymentFragment)
                            .addToBackStack(null).commit();
                }*/


                break;
            case 1:
                Intent intentMobile = new Intent(getActivity(), AddPaymentMobileMoney.class);
                intentMobile.putExtra(Constants.ACCOUNT_NAME, cardModel.getFirstName()+ CardUtils.SPACE_SEPERATOR+cardModel.getLastName());
                getActivity().startActivityForResult(intentMobile, REQUEST_CODE);

                break;

            case 2:

                finishActivityWithXenteCashback();
                break;
        }
    }


    private void finishActivityWithXenteCashback(){
        XenteCashback xenteCasback = new XenteCashback();
        Intent intent = new Intent();
        xenteCasback.setPhoneNumber(cardModel.getPhoneNumber());
        xenteCasback.setDescription("Description for the purchase");
        intent.putExtra(Constants.CASHBACK_MODEL, (Serializable)xenteCasback);
        getActivity().setResult(RESULT_OK, intent);
        getActivity().finish();
    }
}
