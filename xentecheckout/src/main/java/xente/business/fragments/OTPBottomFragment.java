package xente.business.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import xente.business.models.NetworkResponse;
import xente.business.utils.Constants;
import xente.business.xentecheckout.R;

/**
 * Created by macosx on 23/02/2018.
 */

public class OTPBottomFragment  extends BottomSheetDialogFragment implements View.OnClickListener, TextWatcher {

    private Button otpOkButton;
    private Button otpCancelButton;
    private TextView otpText;
    private TextInputLayout optWrapper;
    private EditText otpEditText;
    private String phoneNumber;

    public OTPBottomFragment() {
        // Required empty public constructor
    }

    private OTPFragmentCallback otpFragmentCallback;

    @Override
    public void onClick(View v) {

        int btId = v.getId();
        if(btId == R.id.opt_ok_button){

            if(TextUtils.isEmpty(otpEditText.getText().toString())){
                optWrapper.setError(getString(R.string.emtpy_opt_error));
            }else{

                otpFragmentCallback.UIOTPOkActionClicked(true, otpEditText.getText().toString().trim());
            }

        }else if(btId == R.id.otp_cancel_button){
            otpFragmentCallback.UIOTPOkActionClicked(false, null);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        optWrapper.setError(null);
    }

    public interface OTPFragmentCallback{
        void UIOTPOkActionClicked(boolean clicked, String otp);
    }


    private TextView errorText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null){
            phoneNumber = getArguments().getString(Constants.PHONE_NUMBER);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        otpFragmentCallback = (OTPFragmentCallback)activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.otp_layout, container, false);
        initView(v);
        return v;
    }

    private void initView(View v){

        otpCancelButton = (Button)v.findViewById(R.id.otp_cancel_button);
        otpOkButton = (Button)v.findViewById(R.id.opt_ok_button);
        otpText = (TextView)v.findViewById(R.id.otp_text);
        optWrapper = (TextInputLayout)v.findViewById(R.id.opt_wrapper);
        otpEditText = (EditText)v.findViewById(R.id.opt_edittext);

        String optT = getString(R.string.opt_text, phoneNumber);
        otpText.setText(optT);

        otpOkButton.setOnClickListener(this);
        otpCancelButton.setOnClickListener(this);
        otpEditText.addTextChangedListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
