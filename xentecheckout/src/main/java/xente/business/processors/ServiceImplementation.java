package xente.business.processors;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.UUID;

import xente.business.enums.HttpMethod;
import xente.business.interfaces.Cybersource;
import xente.business.interfaces.NetworkConnectionCallback;
import xente.business.messages.CybersourceRequest;
import xente.business.messages.RequestGenerator;
import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.FLWChargeResponse;
import xente.business.models.MobileNumber;
import xente.business.models.NetworkResponse;
import xente.business.models.XenteCashback;
import xente.business.network.NetworkHandler;
import xente.business.transaction.MetaData;
import xente.business.transaction.TransactionRequest;
import xente.business.utils.Constants;
import xente.business.utils.Helper;

/**
 * Created by Intel World on 20/02/2018.
 */
public abstract class ServiceImplementation extends NetworkResponse {

    private NetworkConnectionCallback networkConnectionCallback;
    private NetworkHandler<ServiceImplementation> networkHandler;
    private BitSet networkFlags;
    private Context context;
    private int CALL_IP2_SERVICE = 1;
    Gson gson;

    public ServiceImplementation(){
        networkFlags = new BitSet();
        gson = new GsonBuilder().setPrettyPrinting().create();

    }

    public <T> void requestFLWCardPayment(CardModel cardModel, CartInstance cartInstance, T callback){

        String flwRequest = RequestGenerator.generateFLWCardChargePayload(cardModel, cartInstance);
        sendRequest(Constants.FLW_TEST_CHARGE_URL, flwRequest, HttpMethod.POST, false, callback);
    }

    public <T> void flutterwaveValidateCardChargeOTP(String transactionRef, int otp, T callback){

        String flwRequest = RequestGenerator.generateFLWValidChargeOPT(transactionRef, otp);
        sendRequest(Constants.FLW_TEXT_VALIDATE_CHARGE_OPT, flwRequest, HttpMethod.POST, false, callback);
    }

    private <T> void sendRequest(String url, Object entity, HttpMethod httpMethod, boolean isCybersourceCall, T callback){

        networkHandler = new NetworkHandler<>(ServiceImplementation.this);
        networkHandler.sendRequest(url, entity, httpMethod.name(), isCybersourceCall);
        networkConnectionCallback = (NetworkConnectionCallback)callback;

    }

    public <T> void requestCybersourceCardPayment(CardModel cardModel, CartInstance cartInstance, T callback){

        //Make two calls one to IP2 and one to flutterwave service, starting with IP2
        CybersourceRequest cybersourceRequest = new CybersourceRequest();
        HashMap<String, String> servicePayload = cybersourceRequest.getCybersourcePayload(cardModel, cartInstance);
        sendRequest(Constants.CYBER_URL_TEST, servicePayload ,HttpMethod.POST, true, callback);
    }



    public void triggerCallback(NetworkResponse networkResponse){
        //The idea is set a bitset and flip it accordingly

        networkConnectionCallback.callback(networkResponse);
    }

    public <T> void postFlutterwaveTransactionToService(String servicePayload, T callback){

        context = (Context)callback;
        //Make two calls one to IP2 and one to flutterwave service, starting with IP2
        networkFlags.set(CALL_IP2_SERVICE);
        sendRequest(Constants.FLW_SERVICE_URL, servicePayload, HttpMethod.POST, false, callback);
    }

    public <T> void postFlutterwaveValidatationToService(String url, T callback){
        sendRequest(url, "", HttpMethod.POST, false,  callback);
    }

    public <T> void postFlutterwaveTokenizedTransactionToService(String servicePayload, T callback){
        String url = "https://7033f201.ngrok.io/FluterwaveService/rest/service/payments/debit?transactionId=75746374736&subscriptionId=7376763848737474&accountId=0787000002&requestId="+ UUID.randomUUID().toString()+"&amount=100.0";
        sendRequest(url, servicePayload, HttpMethod.POST, false,  callback);
    }

    public <T> void requestIP2Payment(CartInstance cartInstance, T callback){

    }
    public <T> void requestIP2Payment(Object object, CartInstance cartInstance, T callback){

        TransactionRequest transactionRequest = gson.fromJson(cartInstance.getProductInformation(), TransactionRequest.class);
        MetaData metaData = transactionRequest.getMetaData();

        MobileNumber mobileNumber = null;
        XenteCashback xenteCashback;
        try{
            mobileNumber = (MobileNumber)object;
            metaData.setPaymentReference(generatePaymentMethodReferences(mobileNumber));
        }catch (ClassCastException e){
            xenteCashback = (XenteCashback)object;
            metaData.setPaymentReference(generatePaymentMethodReferenceCB(xenteCashback));
        }

        transactionRequest.setMetaData(metaData);
        String s = gson.toJson(transactionRequest);
        Log.d("flutterwave", ""+s);
    }


    private HashMap<String, Object> generatePaymentMethodReferences(MobileNumber mobileNumber){

        HashMap<String, Object> paymentMap = new HashMap<>();
        paymentMap.put("PhoneNumber", mobileNumber.getPhoneNumber());
        paymentMap.put("CustomerReference", mobileNumber.getPhoneNumber());

        return paymentMap;

    }

    private HashMap<String, Object> generatePaymentMethodReferenceCB(XenteCashback xenteCashback){
        HashMap<String, Object> paymentMap = new HashMap<>();
        paymentMap.put("PhoneNumber", xenteCashback.getPhoneNumber());
        paymentMap.put("Message", xenteCashback.getDescription());

        return paymentMap;
    }

}
