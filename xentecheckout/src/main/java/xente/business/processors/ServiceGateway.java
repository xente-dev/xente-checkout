package xente.business.processors;

import xente.business.interfaces.CallIP2Service;
import xente.business.interfaces.Cybersource;
import xente.business.interfaces.Flutterwave;
import xente.business.interfaces.FlutterwaveService;
import xente.business.interfaces.NetworkConnectionCallback;
import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.FLWChargeResponse;
import xente.business.models.MobileNumber;
import xente.business.models.NetworkResponse;

/**
 * Created by Intel World on 20/02/2018.
 */
public class ServiceGateway extends ServiceImplementation implements NetworkConnectionCallback,
        Flutterwave, Cybersource , FlutterwaveService, CallIP2Service{

    public ServiceGateway() {
        super();
    }

    @Override
    public <T> void postFlutterwaveValidatationToService(String url, T callback) {
        super.postFlutterwaveValidatationToService(url, callback);
    }

    @Override
    public <T> void postFlutterwaveTokenizedTransactionToService(String servicePayload, T callback) {
        super.postFlutterwaveTokenizedTransactionToService(servicePayload, callback);
    }

    @Override
    public <T> void requestIP2Payment(CartInstance cartInstance, T callback) {
        super.requestIP2Payment(cartInstance, callback);
    }

    @Override
    public <T> void requestIP2Payment(Object object, CartInstance cartInstance, T callback) {
        super.requestIP2Payment(object, cartInstance, callback);
    }

    @Override
    public <T> void flutterwaveValidateCardChargeOTP(String transactionRef, int otp, T callback) {
        super.flutterwaveValidateCardChargeOTP(transactionRef, otp, callback);
    }

    @Override
    public <T> void requestCybersourceCardPayment(CardModel cardModel, CartInstance cartInstance, T callback) {
        super.requestCybersourceCardPayment(cardModel, cartInstance, callback);
    }

    @Override
    public <T> void postFlutterwaveTransactionToService(String servicePayload, T callback) {
        super.postFlutterwaveTransactionToService(servicePayload, callback);
    }

    @Override
    public <T> void requestFLWCardPayment(CardModel cardModel, CartInstance cartInstance, T callback) {
        super.requestFLWCardPayment(cardModel, cartInstance, callback);
    }

    @Override
    public void callback(NetworkResponse networkResponse) {

    }
}
