package xente.business.adapters;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import xente.business.models.SavedPayment;
import xente.business.utils.CardUtils;
import xente.business.utils.Constants;
import xente.business.xentecheckout.R;

/**
 * Created by macosx on 06/03/2018.
 */

public class SavedPaymentAdapter extends BaseAdapter {

    private List<SavedPayment> savedPaymentList;
    private Context context;
    private LayoutInflater inflater;
    private String SLASH_SEPARATOR = "/";

    public SavedPaymentAdapter(List<SavedPayment> savedPaymentList, Context context){
        this.context = context;
        this.savedPaymentList = savedPaymentList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return savedPaymentList.size();
    }

    @Override
    public Object getItem(int position) {
        return savedPaymentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder viewHolder;

        if(v == null){
            v = inflater.inflate(R.layout.saved_payment_item, null);
            viewHolder = new ViewHolder();

            viewHolder.cardNumber = (TextView)v.findViewById(R.id.card_number);
            viewHolder.expiry = (TextView)v.findViewById(R.id.expiry_date);
            viewHolder.logo = (ImageView)v.findViewById(R.id.saved_payment_icon);
            viewHolder.deleteHolder = (LinearLayout)v.findViewById(R.id.delete_card_holder);

            v.setTag(viewHolder);
        }

        ViewHolder holder = (ViewHolder)v.getTag();


        if(savedPaymentList.get(position).getBrand().contains(Constants.VISA)){

            holder.logo.setBackgroundResource(R.drawable.visa_icon);
        }else if(savedPaymentList.get(position).getBrand().equals(CardUtils.MobileNumberProvider.MTN.name())){
            holder.logo.setBackgroundResource(R.drawable.mtn_logo);
        }else if(savedPaymentList.get(position).getBrand().equals(CardUtils.MobileNumberProvider.AIRTEL.name())){
            holder.logo.setBackgroundResource(R.drawable.airtel_logo);
        }

        String maskedCardNumber = context.getString(R.string.card_masked_number, savedPaymentList.get(position).getLast4digits());
        String expiresOn = context.getString(R.string.expires_on, savedPaymentList.get(position).getExpirymonth().concat(SLASH_SEPARATOR).concat(savedPaymentList.get(position).getExpiryyear()));

        if(!savedPaymentList.get(position).isBankCard()){

            String phone = savedPaymentList.get(position).getPhone();
            String subString = phone.substring(phone.length() - 4);
            String maskedNumber = context.getString(R.string.card_masked_number, subString);
            holder.cardNumber.setText(savedPaymentList.get(position).getBrand().concat(CardUtils.SPACE_SEPERATOR).concat(maskedNumber));

        }else{

            holder.cardNumber.setText(savedPaymentList.get(position).getBrand().concat(CardUtils.SPACE_SEPERATOR).concat(maskedCardNumber));
            holder.expiry.setText(expiresOn);
        }


        final int pos = position;
        final String mcn = maskedCardNumber;

        holder.deleteHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String deleteMessage = context.getString(R.string.delete_card, savedPaymentList.get(pos).getBrand().concat(CardUtils.SPACE_SEPERATOR).concat(mcn));
                deleteCardPrompt(deleteMessage, savedPaymentList.get(pos).getCardId());

            }
        });

        if(!savedPaymentList.get(position).isValid()){
           holder.cardNumber.setTextColor(ContextCompat.getColor(context, R.color.brown_color));
        }else if(savedPaymentList.get(position).isValid()){
            holder.cardNumber.setTextColor(ContextCompat.getColor(context, R.color.dark_text_color));
        }

        return v;
    }

    private class ViewHolder{

        TextView cardNumber;
        TextView expiry;
        ImageView logo;
        LinearLayout deleteHolder;

    }

    private void deleteCardPrompt(String message, String cardId){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setCancelable(false);
        builder.show();
    }
}
