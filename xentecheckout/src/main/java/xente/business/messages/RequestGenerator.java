package xente.business.messages;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.FLWChargeResponse;
import xente.business.models.SavedPayment;
import xente.business.utils.AppUtils;
import xente.business.utils.CardUtils;
import xente.business.utils.EncryptorDecryptor;

/**
 * Created by Intel World on 20/02/2018.
 */
public class RequestGenerator {

    /*private static String FLWSECKEY = "FLWSECK-a71fed0a7ae43085f45c316d92ef7340-X";
    private static String FLWPUBKEY = "FLWPUBK-93b1d5a5da999976aeec7754d5e02869-X";*/

    //ZEBRYO
    /*private static String FLWPUBKEY = "FLWPUBK-391da40cc961ab75026d70a710a25df9-X";
    private static String FLWSECKEY = "FLWSECK-c54e63cc3a3bdfb7a94ecb6f803ba6f7-X";*/




    //kyeza
    /*private static String FLWSECKEY = "FLWSECK-d4b18d989f97031909e0805159d1b728-X";
    private static String FLWPUBKEY = "FLWPUBK-d9950cb4524269de7bf411a0439c78e2-X";
*/
    //Immaculate
    private static String FLWPUBKEY= "FLWPUBK-e0fcdaec8f9d13835dde062f929f1195-X";
    private static String FLWSECKEY = "FLWSECK-e2f2cedc067aa5b6cacbb435caa1582a-X";

    public static String generateFLWCardChargePayload(CardModel cardModel, CartInstance cartInstance){

        JSONObject cardParams = new JSONObject();
        try {

            cardParams.put("cardno", cardModel.getCardNumber());
            cardParams.put("cvv", cardModel.getCvv());
            cardParams.put("country", cardModel.getCountry());
            cardParams.put("expirymonth", cardModel.getExpiryMonth());
            cardParams.put("expiryyear", cardModel.getExpiryYear());
            cardParams.put("currency", cartInstance.getCurrency());
            cardParams.put("amount", cartInstance.getAmount().toPlainString());
            cardParams.put("email", cardModel.getEmail());
            cardParams.put("phonenumber", cardModel.getPhoneNumber());
            cardParams.put("firstname", cardModel.getFirstName());
            cardParams.put("lastname", cardModel.getLastName());
            cardParams.put("IP", AppUtils.getIPAddress(true));
            cardParams.put("txRef", cartInstance.getRequestId());
            cardParams.put("redirect_url", "https://7033f201.ngrok.io/FluterwaveService/rest/service/flutterwave/redirect");

            String encryptedKey = EncryptorDecryptor.getKeyRave(RequestGenerator.FLWSECKEY);

            JSONObject payload = new JSONObject();
            payload.put("PBFPubKey", RequestGenerator.FLWPUBKEY);
            payload.put("client", EncryptorDecryptor.encryptData(cardParams.toString(), encryptedKey));
            payload.put("alg", "3DES-24");

            return payload.toString();

        } catch (JSONException e) {
            return null;
        }

    }


    public static String generateFLWValidChargeOPT(String transactionRef, int otp){
        JSONObject object = new JSONObject();
        try {
            object.put("PBFPubKey", RequestGenerator.FLWPUBKEY);
            object.put("transaction_reference", transactionRef);
            object.put("otp", otp);

            return object.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String flutterWaveServicePayload(CardModel cardModel, CartInstance cartInstance){

        JSONObject  servicePayload = new JSONObject();
        try {
            servicePayload.put("add_card", cardModel.isAddCard());
            servicePayload.put("tx_ref", cartInstance.getRequestId());
            servicePayload.put("amount", cartInstance.getAmount().toPlainString());
            servicePayload.put("currency", cartInstance.getCurrency());
            servicePayload.put("customer_phone", cardModel.getPhoneNumber());
            servicePayload.put("customer_email", cardModel.getEmail());
            servicePayload.put("customer_fullname", cardModel.getFirstName().concat(CardUtils.SPACE_SEPERATOR).concat(cardModel.getLastName()));

            return servicePayload.toString();
        } catch (JSONException e) {
            return null;
        }
    }

    public static String flutterwaveTokenizedTransationPayload(SavedPayment savedPayment){
        JSONObject object = new JSONObject();

        try {
            object.put("token", savedPayment.getEmbedToken());
            object.put("currency", "USD");
            object.put("country", "NG");
            object.put("transaction_amount", 100.1);
            object.put("email", savedPayment.getEmail());
            object.put("first_name", savedPayment.getFullName().split(" ")[0]);
            object.put("last_name", savedPayment.getFullName().split(" ")[1]);
            object.put("IP", "192.168.0.1");
            object.put("narration", "payment");
            object.put("card_id", savedPayment.getCardId());

            return object.toString();
        } catch (JSONException e) {
            return null;
        }


    }
}
