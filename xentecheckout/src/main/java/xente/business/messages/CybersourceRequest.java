package xente.business.messages;

import android.content.Context;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import xente.business.models.CardModel;
import xente.business.models.CartInstance;
import xente.business.models.NetworkResponse;
import xente.business.utils.CardUtils;

/**
 * Created by macosx on 16/03/2018.
 */

public class CybersourceRequest {

   /*
    public static String ACCESS_KEY = "f9e8cebecbb93e3894644fd406c425cd";
    public static String PROFILE_ID = "764536C5-771A-4B75-B39A-ED53083E9A31";

    private static final String HMAC_SHA256 = "HmacSHA256";
    private static final String SECRET_KEY = "76b3cb33cb6c4740a4f960173360df4144cea95f9a2d4ea7acc55fa8db5dc1502a1a2f18e2ec4651b72592fc917b5592087a2d4d6546418aac1ccb9afdd99a978bdd944cdc8f4a22b9909820a0b56d1186974d47cfbc4639862b3135a67e211fd60ff6c3ec864eb98331b6ccd4187c6e0b1cb4de710644c798ad76dbdf88ca64";
*/
    private static final String SIGNED_FIELDS = "access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency,payment_method,bill_to_forename,bill_to_surname,bill_to_email,bill_to_phone,bill_to_address_line1,bill_to_address_city,bill_to_address_state,bill_to_address_country,bill_to_address_postal_code";
    private static final String UNSIGNED_FIELDS = "card_type,card_number,card_expiry_date,card_cvv";

    //TEST

    public static String ACCESS_KEY = "9a9d50adbbab35b48518d17fe51fbfb6";
    public static String PROFILE_ID = "F5C33FDB-6DBC-4703-A349-2201961A6A71";

    private static final String HMAC_SHA256 = "HmacSHA256";
    private static final String SECRET_KEY = "10aa96785bb046848a0263594948f70aee8b43055a6d47b6b50a0d143afcf071776dd8e3712b4e71abbfd7d45a46b4092704ddaf6fa34f7bbe483bb9811911196b0f8494b9c54ca6a51e115509dcdcf58123378c8532498a88877717ec6d4ea6e8e0df5eb23f4130ae6b77c63de3563c9e849e2de4cd412397db29bd63fd36df";
    private String DASH_SEPARATOR = "-";

    public HashMap<String,String> getCybersourcePayload( CardModel cardModel, CartInstance cartInstance)
    {


        String cardExpiry = cardModel.getExpiryMonth().concat(DASH_SEPARATOR).concat("20"+cardModel.getExpiryYear());

        HashMap<String, String> h = new HashMap<String, String>();

        h.put("access_key", this.ACCESS_KEY);
        h.put("profile_id", this.PROFILE_ID);
        h.put("transaction_uuid", cartInstance.getRequestId());
        h.put("signed_field_names", SIGNED_FIELDS);
        h.put("unsigned_field_names",UNSIGNED_FIELDS);
        h.put("signed_date_time", getUTCDateTime());
        h.put("locale", "en");
        h.put("transaction_type", "sale");
        h.put("reference_number", UUID.randomUUID().toString().trim());
        h.put("amount", cartInstance.getAmount().toPlainString());
        h.put("currency", cartInstance.getCurrency());
        h.put("payment_method", "card");
        h.put("bill_to_forename", cardModel.getFirstName());
        h.put("bill_to_surname", cardModel.getLastName());
        h.put("bill_to_email", cardModel.getEmail() == null ? "support@intelworld.co.ug" : cardModel.getEmail());
        h.put("bill_to_phone",cardModel.getPhoneNumber());
        h.put("bill_to_address_line1", cardModel.getAddress());
        h.put("bill_to_address_city", cardModel.getCity());
        h.put("bill_to_address_state", cardModel.getState());
        h.put("bill_to_address_country", cardModel.getCountry());
        h.put("bill_to_address_postal_code", cardModel.getPostal());

        h.put("card_type", "001");
        h.put("card_number", cardModel.getCardNumber());
        h.put("card_expiry_date", cardExpiry.length() == 6? "0"+cardExpiry : cardExpiry);
        h.put("card_cvv", cardModel.getCvv());

        String signature = null;
        try {
            signature = sign(h);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        h.put("signature", signature);

        return h;
    }

    private String getUTCDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(new Date());
    }

    public String sign(HashMap params) throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
        return sign(buildDataToSign(params), SECRET_KEY);
    }

    private String sign(String data, String secretKey) throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256);
        Mac mac = Mac.getInstance(HMAC_SHA256);
        mac.init(secretKeySpec);
        byte[] rawHmac = mac.doFinal(data.getBytes("UTF-8"));
        return android.util.Base64.encodeToString(rawHmac, 16) .replace("\n", "");
    }

    private String buildDataToSign(HashMap params) {
        String[] signedFieldNames = String.valueOf(params.get("signed_field_names")).split(",");
        ArrayList<String> dataToSign = new ArrayList<String>();
        for (String signedFieldName : signedFieldNames) {
            dataToSign.add(signedFieldName + "=" + String.valueOf(params.get(signedFieldName)));
        }
        return commaSeparate(dataToSign);
    }

    private String commaSeparate(ArrayList<String> dataToSign) {
        StringBuilder csv = new StringBuilder();
        for (Iterator<String> it = dataToSign.iterator(); it.hasNext(); ) {
            csv.append(it.next());
            if (it.hasNext()) {
                csv.append(",");
            }
        }
        return csv.toString();
    }


}
